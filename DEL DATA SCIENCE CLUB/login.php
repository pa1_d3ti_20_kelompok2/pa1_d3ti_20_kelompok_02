<?php 

require_once("config.php");

if(isset($_POST['login'])){

    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    $role = filter_input(INPUT_POST, 'role', FILTER_SANITIZE_STRING);

    $sql = "SELECT * FROM users WHERE username=:username OR email=:email";
    $stmt = $db->prepare($sql);
    
    // bind parameter ke query
    $params = array(
        ":username" => $username,
        ":email" => $username
        
    );

    $stmt->execute($params);

    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    $sql = "SELECT * FROM users WHERE role=:role";
    $stmt = $db->prepare($sql);
    
    // bind parameter ke query
    $params = array(
        ":role" => $role   
    );

    $stmt->execute($params);

    // $user = $stmt->fetch(PDO::FETCH_ASSOC);



    // jika user terdaftar
    if($user){
        // verifikasi password
        if(password_verify($password, $user["password"])){
            // buat Session
            session_start();
            $_SESSION["user"] = $user;
            // login sukses, alihkan ke halaman timeline
            header("Location: timeline.php");
        }
    }


    $login = mysqli_query($koneksi,"SELECT * FROM users WHERE username='$username' and password='$password'");
// // menyesuaikan sesuai row
$cek = mysqli_num_rows($login);

// cek apakah username ada atau tidak

if($cek > 0) {

  $data = mysqli_fetch_assoc($login);

  // cek jika akun Admin
  if($data['role']=="pengurus") {

    // buat session login dan username
    $_SESSION['username'] = $username;
    $_SESSION['role'] = "pengurus";

    // alihkan ke halaman dashboard admin
    header("location:timeline.php");

  // cek jika akun anggota
}else if($data['role']=="anggota") {

    // buat session login dan username
    $_SESSION['username'] = $username;
    $_SESSION['role'] = "anggota";

    // alihkan ke halaman dashboard anggota
    header("location:timeline.php");

  // jika akun guest
  }
  else if($data['role']=="pembina") {

    // buat session login dan username
    $_SESSION['username'] = $username;
    $_SESSION['role'] = "pembina";

    // alihkan ke halaman dashboard anggota
    header("location:timeline.php");

  }

}

}


?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login to DEL DATA SCIENCE CLUB</title>

    <link rel="stylesheet" href="css/bootstrap.min.css" />
</head>
<body class="bg-light">

<div class="container mt-5">
    <div class="row">
        <div class="col-md-6">

        <p>&larr; <a href="index.php">Home</a>

        <h4>Login ke Del Data Science Club</h4>
        <p>Belum punya akun? <a href="register.php">Daftar di sini</a></p>

        <form action="" method="POST">

            <div class="form-group">
                <label for="username">Username</label>
                <input class="form-control" type="text" name="username" placeholder="Username atau email" />
            </div>


            <div class="form-group">
                <label for="password">Password</label>
                <input class="form-control" type="password" name="password" placeholder="Password" />
            </div>
            <div class="form-group">
                <label for="role">Role</label>
                <input class="form-control" type="text" name="role" placeholder="Role" />
            </div>

            <input type="submit" class="btn btn-success btn-block" name="login" value="Masuk" />

        </form>
            
        </div>

        <div class="col-md-6">
            <!-- isi dengan sesuatu di sini -->
        </div>

    </div>
</div>
    
</body>
</html>