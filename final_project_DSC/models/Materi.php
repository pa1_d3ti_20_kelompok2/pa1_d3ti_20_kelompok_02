<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "materi".
 *
 * @property int $id_materi
 * @property string $judul_materi
 * @property string|null $deskripsi_materi
 * @property string $file_materi
 * @property int $id_kategori
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 *
 * @property Kategori $kategori
 */
class Materi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;

    public static function tableName()
    {
        return 'materi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul_materi', 'file_materi', 'id_kategori','id_setting'], 'required'],
            [['id_kategori', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['judul_materi', 'file_materi'], 'string', 'max' => 255],
            [['deskripsi_materi'], 'string'],
            [['id_kategori'], 'exist', 'skipOnError' => true, 'targetClass' => Kategori::className(), 'targetAttribute' => ['id_kategori' => 'id_kategori']],

            [['file'], 'safe'],
            [['file'], 'file', 'extensions'=>'jpg, jpeg, png, tif, pdf, docx, doc, zip, rar'],
            [['file'], 'file', 'maxSize' => 1024 * 1024 * 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_materi' => 'ID Materi',
            'judul_materi' => 'Judul Materi',
            'deskripsi_materi' => 'Deskripsi Materi',
            'file_materi' => 'File Materi',
            'id_kategori' => 'ID Kategori',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'id_setting' => 'ID Setting',
        ];
    }

    public function behaviors()
    {
       return [
           'timestamp' => [
               'class' => TimestampBehavior::className(),
               'attributes' => [
                   ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                   ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
               ],
               'value' => new Expression('NOW()'),
           ],

       ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if(ActiveRecord::EVENT_BEFORE_INSERT){
            $this->created_by = Yii::$app->user->identity->id;
            $this->updated_by = Yii::$app->user->identity->id;
        }else if(ActiveRecord::EVENT_BEFORE_UPDATE){
            $this->updated_by = Yii::$app->user->identity->id;
        }

        return true;
    }

    /**
     * Gets query for [[Kategori]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKategori()
    {
        return $this->hasOne(Kategori::className(), ['id_kategori' => 'id_kategori']);
    }

    public function getSetting()
    {
        return $this->hasOne(Setting::className(), ['id_setting' => 'id_setting']);
    }
}
