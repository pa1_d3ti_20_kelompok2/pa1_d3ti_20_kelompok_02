<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;
/**
 * This is the model class for table "tantangan".
 *
 * @property int $id_tantangan
 * @property string $judul_tantangan
 * @property string|null $deskripsi_tantangan
 * @property string $waktu_buka
 * @property string $waktu_tutup
 * @property string $file_tantangan
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 *
 * @property SubmitTantangan[] $submitTantangans
 * @property TUser[] $users
 */
class Tantangan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */

    public $file;

    public static function tableName()
    {
        return 'tantangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['judul_tantangan', 'waktu_buka', 'waktu_tutup', 'file_tantangan','id_setting'], 'required'],
            [['waktu_buka', 'waktu_tutup', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by','id_setting'], 'integer'],
            [['judul_tantangan', 'file_tantangan'], 'string', 'max' => 255],
            [['deskripsi_tantangan'], 'string'],

            [['file'], 'safe'],
            [['file'], 'file', 'extensions'=>'jpg, jpeg, png, tif, pdf, docx, doc, zip, rar'],
            [['file'], 'file', 'maxSize' => 1024 * 1024 * 1024 * 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tantangan' => 'Id Tantangan',
            'judul_tantangan' => 'Judul Tantangan',
            'deskripsi_tantangan' => 'Deskripsi Tantangan',
            'waktu_buka' => 'Waktu Buka',
            'waktu_tutup' => 'Waktu Tutup',
            'file_tantangan' => 'File Tantangan',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'id_setting' => 'ID Setting',
        ];
    }

    public function behaviors()
    {
       return [
           'timestamp' => [
               'class' => TimestampBehavior::className(),
               'attributes' => [
                   ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                   ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
               ],
               'value' => new Expression('NOW()'),
           ],

       ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if(ActiveRecord::EVENT_BEFORE_INSERT){
            $this->created_by = Yii::$app->user->identity->id;
            $this->updated_by = Yii::$app->user->identity->id;
        }else if(ActiveRecord::EVENT_BEFORE_UPDATE){
            $this->updated_by = Yii::$app->user->identity->id;
        }

        return true;
    }

    /**
     * Gets query for [[SubmitTantangans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubmitTantangans()
    {
        return $this->hasMany(SubmitTantangan::className(), ['id_tantangan' => 'id_tantangan']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(TUser::className(), ['id' => 'id_user'])->viaTable('submit_tantangan', ['id_tantangan' => 'id_tantangan']);
    }
}
