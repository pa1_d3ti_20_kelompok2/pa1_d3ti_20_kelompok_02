<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProgramKerja;

/**
 * ProgramKerjaSearch represents the model behind the search form of `app\models\ProgramKerja`.
 */
class ProgramKerjaSearch extends ProgramKerja
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_program_kerja', 'id_setting', 'created_by', 'updated_by', 'active'], 'integer'],
            [['nama_program_kerja', 'deskripsi_program_kerja', 'tanggal_awal', 'tanggal_akhir', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProgramKerja::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id_program_kerja'=>SORT_DESC,
                    
                ]
            ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_program_kerja' => $this->id_program_kerja,
            'tanggal_awal' => $this->tanggal_awal,
            'tanggal_akhir' => $this->tanggal_akhir,
            'id_setting' => $this->id_setting,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
            'active' => $this->active,
        ]);

        $query->andFilterWhere(['like', 'nama_program_kerja', $this->nama_program_kerja])
            ->andFilterWhere(['like', 'deskripsi_program_kerja', $this->deskripsi_program_kerja]);

        return $dataProvider;
    }
}
