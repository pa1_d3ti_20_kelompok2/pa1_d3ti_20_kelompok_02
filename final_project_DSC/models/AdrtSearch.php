<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Adrt;

/**
 * AdrtSearch represents the model behind the search form of `app\models\Adrt`.
 */
class AdrtSearch extends Adrt
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_adrt', 'created_by', 'updated_by'], 'integer'],
            [['deskripsi_adrt', 'file_adrt', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Adrt::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_adrt' => $this->id_adrt,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'deskripsi_adrt', $this->deskripsi_adrt])
            ->andFilterWhere(['like', 'file_adrt', $this->file_adrt]);

        return $dataProvider;
    }
}
