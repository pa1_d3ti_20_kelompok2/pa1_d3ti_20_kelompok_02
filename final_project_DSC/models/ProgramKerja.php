<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "program_kerja".
 *
 * @property int $id_program_kerja
 * @property string $nama_program_kerja
 * @property string|null $deskripsi_program_kerja
 * @property string $tanggal_awal
 * @property string $tanggal_akhir
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $active
 */
class ProgramKerja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;

    public static function tableName()
    {
        return 'program_kerja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_program_kerja', 'tanggal_awal', 'tanggal_akhir'], 'required'],
            [['tanggal_awal', 'tanggal_akhir', 'created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by', 'active'], 'integer'],
            [['nama_program_kerja','file_program_kerja'], 'string', 'max' => 255],
            [['deskripsi_program_kerja'], 'string'],

            [['file'], 'safe'],
            [['file'], 'file', 'extensions'=>'jpg, jpeg, png, tif, pdf, docx, doc'],
            [['file'], 'file', 'maxSize' => 1024 * 1024 * 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_program_kerja' => 'ID Program Kerja',
            'nama_program_kerja' => 'Nama Program Kerja',
            'deskripsi_program_kerja' => 'Deskripsi Program Kerja',
            'tanggal_awal' => 'Tanggal Awal',
            'tanggal_akhir' => 'Tanggal Akhir',
            'file_program_kerja' => 'File Program Kerja',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'active' => 'Active',
        ];
    }

    public function behaviors()
    {
       return [
           'timestamp' => [
               'class' => TimestampBehavior::className(),
               'attributes' => [
                   ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                   ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
               ],
               'value' => new Expression('NOW()'),
           ],

       ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if(ActiveRecord::EVENT_BEFORE_INSERT){
            $this->created_by = Yii::$app->user->identity->id;
            $this->updated_by = Yii::$app->user->identity->id;
        }else if(ActiveRecord::EVENT_BEFORE_UPDATE){
            $this->updated_by = Yii::$app->user->identity->id;
        }

        return true;
    }
}
