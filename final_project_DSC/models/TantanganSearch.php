<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Tantangan;

/**
 * TantanganSearch represents the model behind the search form of `app\models\Tantangan`.
 */
class TantanganSearch extends Tantangan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tantangan', 'id_setting', 'created_by', 'updated_by'], 'integer'],
            [['judul_tantangan', 'deskripsi_tantangan', 'waktu_buka', 'waktu_tutup', 'file_tantangan', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tantangan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tantangan' => $this->id_tantangan,
            'waktu_buka' => $this->waktu_buka,
            'waktu_tutup' => $this->waktu_tutup,
            'id_setting' => $this->id_setting,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'judul_tantangan', $this->judul_tantangan])
            ->andFilterWhere(['like', 'deskripsi_tantangan', $this->deskripsi_tantangan])
            ->andFilterWhere(['like', 'file_tantangan', $this->file_tantangan]);

        return $dataProvider;
    }
}
