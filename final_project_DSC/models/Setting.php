<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting".
 *
 * @property int $id_setting
 * @property string $nama_aplikasi
 * @property string|null $deskripsi_aplikasi
 * @property int $tahun_aktif
 * @property string|null $logo_aplikasi
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 * @property int|null $active
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_aplikasi', 'tahun_aktif'], 'required'],
            [['tahun_aktif', 'created_by', 'updated_by', 'active'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['nama_aplikasi', 'logo_aplikasi'], 'string', 'max' => 255],
            [['deskripsi_aplikasi'], 'string', 'max' => 8000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_setting' => 'Id Setting',
            'nama_aplikasi' => 'Nama Aplikasi',
            'deskripsi_aplikasi' => 'Deskripsi Aplikasi',
            'tahun_aktif' => 'Tahun Aktif',
            'logo_aplikasi' => 'Logo Aplikasi',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'active' => 'Active',
        ];
    }
}
