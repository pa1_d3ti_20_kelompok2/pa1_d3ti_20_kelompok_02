<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_user".
 *
 * @property int $id
 * @property string $username
 * @property string|null $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property int $status
 * @property int $created_at
 * @property int $updated_at
 * @property string|null $id_lokasi
 * @property string|null $inisial_user
 * @property string|null $nama_user
 * @property string|null $telp
 * @property string|null $foto
 * @property string|null $alamat
 *
 * @property HasilVoting[] $hasilVotings
 * @property MVoting[] $votings
 */
class TUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['username', 'auth_key', 'password_hash', 'password_reset_token', 'email', 'id_lokasi', 'inisial_user', 'nama_user', 'telp', 'foto'], 'string', 'max' => 255],
            [['alamat'], 'string', 'max' => 500],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'id_lokasi' => 'Id Lokasi',
            'inisial_user' => 'Inisial User',
            'nama_user' => 'Nama Pengguna',
            'telp' => 'Telp',
            'foto' => 'Foto',
            'alamat' => 'Alamat',
        ];
    }

    /**
     * Gets query for [[HasilVotings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHasilVotings()
    {
        return $this->hasMany(HasilVoting::className(), ['id_user' => 'id']);
    }

    /**
     * Gets query for [[Votings]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVotings()
    {
        return $this->hasMany(MVoting::className(), ['id_voting' => 'id_voting'])->viaTable('hasil_voting', ['id_user' => 'id']);
    }
}
