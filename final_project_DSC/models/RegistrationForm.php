<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\User;


/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class RegistrationForm extends Model
{
    public $fullname;
    public $role;
    public $username;
    public $password;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password','fullname','role'], 'required'],
            [['username', 'password'], 'required','on' => 'register'],
            [['fullname', 'role'], 'required','on' => 'register'],
            
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    public function scenarios(){
        $scenarios = parent::scenarios();
        $scenarios['register'] = ['username','password','fullname','role','last_login'];
        return $scenarios;
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'password' => 'Password',
            'last_login' => 'Last Login',
            'role' => 'Role',
            'fullname' => 'Full Name',
        ];
    }

    public static function hashPassword($_password){
        return (Yii::$app->getSecurity()->generatePasswordHash($_password));
    }

    public function generateAuthKey()
    {
        return Yii::$app->security->generateRandomString();
    }

    public function register(){
        if ($this->validate()) {
            $account = new Account();
            $account->username = $this->username;
            $account->password = self::hashPassword($this->password);
            $account->fullname = $this->fullname;
            $account->role = $this->role;
            $account->auth_key = self::generateAuthKey();
            if($account->save()){
                return $account;
            }else{
                print_r($account->errors);
            }

            return false;
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 : 0);
        }
        return false;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {
        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}
