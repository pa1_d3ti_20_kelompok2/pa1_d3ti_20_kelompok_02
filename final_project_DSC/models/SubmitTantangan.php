<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "submit_tantangan".
 *
 * @property int $id_tantangan
 * @property int $id_user
 * @property string $file_submit
 * @property string|null $created_at
 * @property int|null $created_by
 * @property string|null $updated_at
 * @property int|null $updated_by
 *
 * @property Tantangan $tantangan
 * @property TUser $user
 */
class SubmitTantangan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;

    public static function tableName()
    {
        return 'submit_tantangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tantangan', 'id_user', 'file_submit'], 'required'],
            [['id_tantangan', 'id_user', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['file_submit'], 'string', 'max' => 255],
            [['nilai'], 'number'],
            [['id_tantangan', 'id_user'], 'unique', 'targetAttribute' => ['id_tantangan', 'id_user']],
            [['id_tantangan'], 'exist', 'skipOnError' => true, 'targetClass' => Tantangan::className(), 'targetAttribute' => ['id_tantangan' => 'id_tantangan']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => TUser::className(), 'targetAttribute' => ['id_user' => 'id']],

            [['file'], 'safe'],
            [['file'], 'file', 'extensions'=>'jpg, jpeg, png, pdf, docx, doc, zip, rar'],
            [['file'], 'file', 'maxSize' => 1024 * 1024 * 1024 * 1024],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tantangan' => 'ID Tantangan',
            'id_user' => 'ID User',
            'file_submit' => 'File Submit',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
            'nilai' => 'Nilai',
        ];
    }

    public function behaviors()
    {
       return [
           'timestamp' => [
               'class' => TimestampBehavior::className(),
               'attributes' => [
                   ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                   ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
               ],
               'value' => new Expression('NOW()'),
           ],

       ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }
        if(ActiveRecord::EVENT_BEFORE_INSERT){
            $this->created_by = Yii::$app->user->identity->id;
            $this->updated_by = Yii::$app->user->identity->id;
        }else if(ActiveRecord::EVENT_BEFORE_UPDATE){
            $this->updated_by = Yii::$app->user->identity->id;
        }

        return true;
    }

    /**
     * Gets query for [[Tantangan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTantangan()
    {
        return $this->hasOne(Tantangan::className(), ['id_tantangan' => 'id_tantangan']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(TUser::className(), ['id' => 'id_user']);
    }
}
