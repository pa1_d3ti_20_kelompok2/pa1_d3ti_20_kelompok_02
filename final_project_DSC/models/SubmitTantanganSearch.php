<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\SubmitTantangan;

/**
 * SubmitTantanganSearch represents the model behind the search form of `app\models\SubmitTantangan`.
 */
class SubmitTantanganSearch extends SubmitTantangan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tantangan', 'id_user', 'created_by', 'updated_by'], 'integer'],
            [['file_submit', 'created_at', 'updated_at'], 'safe'],
            [['nilai'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = SubmitTantangan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_tantangan' => $this->id_tantangan,
            'id_user' => $this->id_user,
            'nilai' => $this->nilai,
            'created_at' => $this->created_at,
            'created_by' => $this->created_by,
            'updated_at' => $this->updated_at,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'file_submit', $this->file_submit]);

        return $dataProvider;
    }
}
