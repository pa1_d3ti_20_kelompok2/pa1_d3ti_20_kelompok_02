<?php

namespace app\controllers;

use Yii;
use app\models\SubmitTantangan;
use app\models\SubmitTantanganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SubmitTantanganController implements the CRUD actions for SubmitTantangan model.
 */
class SubmitTantanganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all SubmitTantangan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SubmitTantanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single SubmitTantangan model.
     * @param integer $id_tantangan
     * @param integer $id_user
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_tantangan, $id_user)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_tantangan, $id_user),
        ]);
    }

    /**
     * Creates a new SubmitTantangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_tantangan, $id_user)
    {
        $model = new SubmitTantangan();

        if ($model->load(Yii::$app->request->post())) {
            $model->id_tantangan = $id_tantangan;
            $model->id_user = $id_user;

            $file = UploadedFile::getInstance($model, 'file');
            if (!is_null($file)) {
                $tmp = explode('.', $file->name);
                $ext_foto = end($tmp);
                //$ext_foto = end((explode(".", $image_foto->name)));
                  // generate a unique file name to prevent duplicate filenames
                $model->file_submit = Yii::$app->security->generateRandomString().".{$ext_foto}";
                //$model->file_submit = $file;
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/upload/submit_tantangan/';
                $path_file = Yii::$app->params['uploadPath'] . $model->file_submit;
                
                $file->saveAs($path_file);  
            }
            
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'File Tantangan Berhasil Disubmit');
            } else {
                Yii::$app->session->setFlash('danger', ' FileTantangan Gagal Disubmit');
            }

            return $this->redirect(['tantangan/view','id'=>$id_tantangan]);
        } else {
            
            return $this->redirect(['tantangan/view','id'=>$id_tantangan]);
        }


    }

    /**
     * Updates an existing SubmitTantangan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id_tantangan
     * @param integer $id_user
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_tantangan, $id_user)
    {
        $model = $this->findModel($id_tantangan, $id_user);

        if ($model->load(Yii::$app->request->post())) {

            $file = UploadedFile::getInstance($model, 'file');
            if (!is_null($file)) {
                $tmp = explode('.', $file->name);
                $ext_foto = end($tmp);
                //$ext_foto = end((explode(".", $image_foto->name)));
                  // generate a unique file name to prevent duplicate filenames
                $model->file_submit = Yii::$app->security->generateRandomString().".{$ext_foto}";
                //$model->file_submit = $file;
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/upload/submit_tantangan/';
                $path_file = Yii::$app->params['uploadPath'] . $model->file_submit;
                
                $file->saveAs($path_file);  
            }
            
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'File Tantangan Berhasil Disubmit');
            } else {
                Yii::$app->session->setFlash('danger', ' FileTantangan Gagal Disubmit');
            }

            return $this->redirect(['tantangan/view','id'=>$id_tantangan]);
        } else {
            
            return $this->redirect(['tantangan/view','id'=>$id_tantangan]);
        }

    }

    /**
     * Deletes an existing SubmitTantangan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id_tantangan
     * @param integer $id_user
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_tantangan, $id_user)
    {
        $this->findModel($id_tantangan, $id_user)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SubmitTantangan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id_tantangan
     * @param integer $id_user
     * @return SubmitTantangan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_tantangan, $id_user)
    {
        if (($model = SubmitTantangan::findOne(['id_tantangan' => $id_tantangan, 'id_user' => $id_user])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
