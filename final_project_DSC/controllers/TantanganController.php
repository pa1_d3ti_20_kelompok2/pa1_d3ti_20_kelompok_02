<?php

namespace app\controllers;

use Yii;
use app\models\Tantangan;
use app\models\TantanganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Setting;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\modules\systemxadmin\models\User;
use app\modules\systemxadmin\models\AuthAssignment;
use app\modules\systemxadmin\models\AuthItem;
use app\modules\systemxadmin\models\UserSearch;
use app\models\SubmitTantangan;
/**
 * TantanganController implements the CRUD actions for Tantangan model.
 */
class TantanganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tantangan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $setting = Setting::find()->where(['active'=>10])->orderBy(['id_setting' => 'DESC'])->one();

        $arrayTahun = ArrayHelper::map(Setting::find()->orderBy(['id_setting' => 'DESC'])->all(),'id_setting','tahun_aktif');

        $tantangan = Tantangan::find()->where(['id_setting' => $setting->id_setting])->orderBy(['id_tantangan'=>SORT_DESC])->all();

        $pembina = User::find()->join('JOIN','auth_assignment','auth_assignment.user_id = t_user.id')->where(['auth_assignment.item_name'=>'PEMBINA'])->all();
        $countAnggota =  User::find()->join('JOIN','auth_assignment','auth_assignment.user_id = t_user.id')->where(['auth_assignment.item_name'=>'ANGGOTA'])->count();

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchAnggota(Yii::$app->request->queryParams);

        $user = Yii::$app->user->identity;
        $role = AuthAssignment::find()->where(['user_id'=>$user->id])->one();

        return $this->render('index', [
            'setting' => $setting,
            'tantangan' => $tantangan,
            'arrayTahun' => $arrayTahun,
            'pembina' => $pembina,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'countAnggota' => $countAnggota,
            'role' => $role,
        ]);
    }

    /**
     * Displays a single Tantangan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $model_user = User::find()->where(['id'=>$model->updated_by])->one();

        $user = Yii::$app->user->identity;
        $role = AuthAssignment::find()->where(['user_id'=>$user->id])->one();

        $submit_tantangan = SubmitTantangan::find()->where(['id_tantangan'=>$id])->andWhere(['id_user'=>$user->id])->one();
        if (is_null($submit_tantangan)) {
            $model_submit = new SubmitTantangan();
        }else{
            $model_submit = $submit_tantangan;
        }

        return $this->render('view', [
            'model' => $model,
            'model_user' => $model_user,
            'role' => $role,
            'model_submit' => $model_submit,
            'user' => $user,
            'submit_tantangan' => $submit_tantangan,
        ]);
    }

    /**
     * Creates a new Tantangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tantangan();

        $setting = Setting::find()->where(['active'=>10])->orderBy(['id_setting' => 'DESC'])->one();
        
        if ($model->load(Yii::$app->request->post())) {
             
            $file = UploadedFile::getInstance($model, 'file');
            if (!is_null($file)) {
                $tmp = explode('.', $file->name);
                $ext_foto = end($tmp);
                //$ext_foto = end((explode(".", $image_foto->name)));
                  // generate a unique file name to prevent duplicate filenames
                $model->file_tantangan = Yii::$app->security->generateRandomString().".{$ext_foto}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/upload/tantangan/';
                $path_file = Yii::$app->params['uploadPath'] . $model->file_tantangan;
                
                $file->saveAs($path_file);  
            }
            $model->id_setting = $setting->id_setting;
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Tantangan Berhasil Diupload');
            } else {
                Yii::$app->session->setFlash('danger', 'Tantangan Gagal Diupload');
            }

            return $this->redirect(['index']);
        } else {
            
            return $this->render('create', [
                'model' => $model,
                
            ]);
        }
    }

    /**
     * Updates an existing Tantangan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $setting = Setting::find()->where(['active'=>10])->orderBy(['id_setting' => 'DESC'])->one();
        
        if ($model->load(Yii::$app->request->post())) {
             
            $file = UploadedFile::getInstance($model, 'file');
            if (!is_null($file)) {
                $tmp = explode('.', $file->name);
                $ext_foto = end($tmp);
                //$ext_foto = end((explode(".", $image_foto->name)));
                  // generate a unique file name to prevent duplicate filenames
                $model->file_tantangan = Yii::$app->security->generateRandomString().".{$ext_foto}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/upload/tantangan/';
                $path_file = Yii::$app->params['uploadPath'] . $model->file_tantangan;
                
                $file->saveAs($path_file);  
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Tantangan Berhasil Diupdate');
            } else {
                Yii::$app->session->setFlash('danger', 'Tantangan Gagal Diupdate');
            }

            return $this->redirect(['index']);
        } else {
            
            return $this->render('update', [
                'model' => $model,
                
            ]);
        }
    }

    /**
     * Deletes an existing Tantangan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tantangan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tantangan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tantangan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /*
        Fungsi Untuk Melihat Status Submit Tantangan
    */

    public function actionStatusSubmit($id_tantangan)
    {

        $model = User::find()->join('JOIN','auth_assignment','auth_assignment.user_id = t_user.id')->where(['auth_assignment.item_name'=>'ANGGOTA']);

        $tantangan = $this->findModel($id_tantangan);
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchListUser($model,Yii::$app->request->queryParams);

        return $this->render('status_submit', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tantangan' => $tantangan,
        ]);
    }

    public function actionRekapitulasiNilai($id_tantangan)
    {

        $model = User::find()->join('JOIN','auth_assignment','auth_assignment.user_id = t_user.id')->where(['auth_assignment.item_name'=>'ANGGOTA']);
        $tantangan = $this->findModel($id_tantangan);
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchListUser($model,Yii::$app->request->queryParams);

        return $this->render('rekapitulasi_nilai', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tantangan' => $tantangan,
        ]);
    }

    public function actionUpdateNilaiTantangan($id_tantangan,$id_user)
    {

        $model = SubmitTantangan::find()->where(['id_tantangan'=>$id_tantangan])->andWhere(['id_user'=>$id_user])->one();

        if ($model->load(Yii::$app->request->post())) {
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Nilai Berhasil Disimpan');
            } else {
                Yii::$app->session->setFlash('danger', 'Nilai Gagal Disimpan');
            }

            return $this->redirect(['status-submit','id_tantangan'=>$id_tantangan]);
        } else {
            
            return $this->renderAjax('update_nilai_tantangan', [
                'model' => $model,
                
            ]);
        }
    }


}
