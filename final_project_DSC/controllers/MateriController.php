<?php

namespace app\controllers;

use Yii;
use app\models\Materi;
use app\models\MateriSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Setting;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\models\Kategori;
use app\modules\systemxadmin\models\User;
use app\modules\systemxadmin\models\AuthAssignment;
use app\modules\systemxadmin\models\AuthItem;
use app\modules\systemxadmin\models\UserSearch;

/**
 * MateriController implements the CRUD actions for Materi model.
 */
class MateriController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Materi models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MateriSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Materi model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $model_user = User::find()->where(['id'=>$model->updated_by])->one();

        $user = Yii::$app->user->identity;
        $role = AuthAssignment::find()->where(['user_id'=>$user->id])->one();

        return $this->render('view', [
            'model' => $model,
            'model_user' => $model_user,
            'role' => $role,
        ]);
    }

    /**
     * Creates a new Materi model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_kategori)
    {
        $model = new Materi();
        $setting = Setting::find()->where(['active'=>10])->orderBy(['id_setting' => 'DESC'])->one();
        $kategori = Kategori::find()->where(['id_kategori' =>$id_kategori])->one();

        if ($model->load(Yii::$app->request->post())) {
             
            $file = UploadedFile::getInstance($model, 'file');
            if (!is_null($file)) {
                $tmp = explode('.', $file->name);
                $ext_foto = end($tmp);
                //$ext_foto = end((explode(".", $image_foto->name)));
                  // generate a unique file name to prevent duplicate filenames
                $model->file_materi = Yii::$app->security->generateRandomString().".{$ext_foto}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/upload/materi/';
                $path_file = Yii::$app->params['uploadPath'] . $model->file_materi;
                
                $file->saveAs($path_file);  
            }
            $model->id_setting = $setting->id_setting;
            $model->id_kategori = $id_kategori;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Materi Berhasil Diupload');
            } else {
                Yii::$app->session->setFlash('error', 'Materi Gagal Diupload');
            }

            return $this->redirect(['view-materi','id_kategori' =>$id_kategori]);
        } else {
            
            return $this->render('create', [
                'model' => $model,
                'kategori' =>$kategori,
            ]);
        }
    }

    /**
     * Updates an existing Materi model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        //$model = new Materi();
        $setting = Setting::find()->where(['active'=>10])->orderBy(['id_setting' => 'DESC'])->one();
        $kategori = Kategori::find()->where(['id_kategori' =>$model->id_kategori])->one();

        if ($model->load(Yii::$app->request->post())) {
             
            $file = UploadedFile::getInstance($model, 'file');
            if (!is_null($file)) {
                $tmp = explode('.', $file->name);
                $ext_foto = end($tmp);
                //$ext_foto = end((explode(".", $image_foto->name)));
                  // generate a unique file name to prevent duplicate filenames
                $model->file_materi = Yii::$app->security->generateRandomString().".{$ext_foto}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/upload/materi/';
                $path_file = Yii::$app->params['uploadPath'] . $model->file_materi;
                
                $file->saveAs($path_file);  
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Materi Berhasil Diupdate');
            } else {
                Yii::$app->session->setFlash('error', 'Materi Gagal Diupdate');
            }

            return $this->redirect(['view-materi','id_kategori' =>$model->id_kategori]);
        } else {
            
            return $this->render('update', [
                'model' => $model,
                'kategori' =>$kategori,
            ]);
        }
    }

    /**
     * Deletes an existing Materi model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Materi model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Materi the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Materi::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /*
        Fungsi Untuk View Semua Materi Yang Diupload Per Kategori
    */
    public function actionViewMateri($id_kategori)
    {

        $setting = Setting::find()->where(['active'=>10])->orderBy(['id_setting' => 'DESC'])->one();

        $arrayTahun = ArrayHelper::map(Setting::find()->orderBy(['id_setting' => 'DESC'])->all(),'id_setting','tahun_aktif');

        $materi = Materi::find()->where(['id_setting' => $setting->id_setting])->andWhere(['id_kategori' => $id_kategori])->all();

        $kategori = Kategori::find()->where(['id_kategori' =>$id_kategori])->one();

        $pembina = User::find()->join('JOIN','auth_assignment','auth_assignment.user_id = t_user.id')->where(['auth_assignment.item_name'=>'PEMBINA'])->all();
        $countAnggota =  User::find()->join('JOIN','auth_assignment','auth_assignment.user_id = t_user.id')->where(['auth_assignment.item_name'=>'ANGGOTA'])->count();

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchAnggota(Yii::$app->request->queryParams);

        $user = Yii::$app->user->identity;
        $role = AuthAssignment::find()->where(['user_id'=>$user->id])->one();


        return $this->render('view_materi', [
            'setting' => $setting,
            'materi' => $materi,
            'arrayTahun' => $arrayTahun,
            'kategori' =>$kategori,
            'pembina' => $pembina,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'countAnggota' => $countAnggota,
            'role' => $role,

        ]);
    }

}
