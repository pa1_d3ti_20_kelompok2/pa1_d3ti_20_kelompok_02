<?php

namespace app\controllers;

use Yii;
use app\models\Adrt;
use app\models\AdrtSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\systemxadmin\models\User;
use app\modules\systemxadmin\models\AuthAssignment;
use app\modules\systemxadmin\models\AuthItem;
use app\modules\systemxadmin\models\UserSearch;
use yii\web\UploadedFile;
/**
 * AdrtController implements the CRUD actions for Adrt model.
 */
class AdrtController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Adrt models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AdrtSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Adrt model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView()
    {
        $model = Adrt::find()->orderBy(['id_adrt'=>'DESC'])->one();

        $model_user = User::find()->where(['id'=>$model->updated_by])->one();

        $user = Yii::$app->user->identity;
        $role = AuthAssignment::find()->where(['user_id'=>$user->id])->one();

        return $this->render('view', [
            'model' => $model,
            'model_user' => $model_user,
            'role' => $role,
        ]);
    }

    /**
     * Creates a new Adrt model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Adrt();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_adrt]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Adrt model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
             
            $file = UploadedFile::getInstance($model, 'file');
            if (!is_null($file)) {
                $tmp = explode('.', $file->name);
                $ext_foto = end($tmp);
                $model->file_adrt = Yii::$app->security->generateRandomString().".{$ext_foto}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/upload/adrt/';
                $path_file = Yii::$app->params['uploadPath'] . $model->file_adrt;
                
                $file->saveAs($path_file);  
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'ADRT Berhasil Diupdate');
            } else {
                Yii::$app->session->setFlash('error', 'ADRT Gagal Diupdate');
            }

            return $this->redirect(['view']);
        } else {
            
            return $this->render('update', [
                'model' => $model,
                
            ]);
        }
    }

    /**
     * Deletes an existing Adrt model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Adrt model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Adrt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Adrt::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
