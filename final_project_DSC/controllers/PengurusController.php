<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\systemxadmin\models\User;
use app\modules\systemxadmin\models\AuthAssignment;
use app\modules\systemxadmin\models\AuthItem;
use app\modules\systemxadmin\models\UserSearch;

/**
 * AdrtController implements the CRUD actions for Adrt model.
 */
class PengurusController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Adrt models.
     * @return mixed
     */
    public function actionHomeDaftarUser()
    {
        $role = AuthItem::find()->where(['type'=>1])->andWhere(['<>','name', 'ADMINISTRATOR'])->all();

        return $this->render('home_daftar_user', [
            'role' => $role,
        ]);
    }

    public function actionListUser($role)
    {
        $model_role = AuthItem::find()->where(['type'=>1])->andWhere(['name' => $role])->one();
        $model = User::find()->join('JOIN','auth_assignment','auth_assignment.user_id = t_user.id')->where(['auth_assignment.item_name'=>$role]);

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchListUser($model,Yii::$app->request->queryParams);

        return $this->render('list_user', [
            'model_role' => $model_role,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionListVerifikasiPendaftar()
    {
        
        $model = User::find()->join('JOIN','auth_assignment','auth_assignment.user_id = t_user.id')->where(['auth_assignment.item_name'=>'PESERTA']);

        $searchModel = new UserSearch();
        $dataProvider = $searchModel->searchListUser($model,Yii::$app->request->queryParams);

        return $this->render('list_verifikasi_pendaftar', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionVerifikasipendaftar($id)
    {
        
        $model = AuthAssignment::find()->where(['user_id' => $id])->one();
        $user = User::find()->where(['id' => $id])->one();

        $model->item_name = "ANGGOTA";
        $model->save();
        if ($model->save()) {
            $email = \Yii::$app->mailer->compose()
            ->setFrom(['henny.panjaitan3004@gmail.com'=>'Del Data Science Club'])
            ->setTo($user->email)
            ->setSubject('Informasi Pemberitahuan Verifikasi Jadi Anggota')
            ->setHtmlBody('
                <b>Terima Kasih Buat Telah Melakukan Pendaftaran Pada Del Data Science Club</b>
                <br><br>
                <b>SELAMAT!!! Anda dinyatakan sebagai "ANGGOTA" Del Data Science Club</b>')
            ->send();

            if($email){
                Yii::$app->session->setFlash('success', "Email Verifikasi Telah Terkirim Ke Email Pendaftar");
            }
            
        } else {
            Yii::$app->session->setFlash('danger', "Gagal Verifikasi");
        }

        return $this->redirect(['list-verifikasi-pendaftar']);
        
    }



    
}
