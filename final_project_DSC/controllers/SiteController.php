<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RegistrationForm;
//use app\models\User;
//use hscstudio\mimin\models\AuthAssignment;
use app\models\TUser;
use app\modules\systemxadmin\models\User;
use app\modules\systemxadmin\models\AuthAssignment;
use app\modules\systemxadmin\models\AuthItem;
use app\modules\systemxadmin\models\UserSearch;
use yii\web\UploadedFile;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        
        if (!Yii::$app->user->isGuest) {
            //return $this->goHome();
            return $this->redirect(['site/index']);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
            //return $this->redirect(['site/index']);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);



    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /*
        Fungsi Register : Untuk menambahkan user baru
    */
    public function actionRegister()
    {
        
        $model = new User();
        $modelRole = new AuthAssignment();

        if ($model->load(Yii::$app->request->post())) {
            $password = $model->password_hash;
            //print_r($password);die;
            $model->setPassword($password);
            $model->status = 10;
            if ($model->save()) {
                $user = TUser::find()->where(['username' => $model->username])->andWhere(['email'=>$model->email])->one();
                $modelRole->item_name = "PESERTA";
                $modelRole->user_id = $user->id;

                if($modelRole->save()){
                    $email = \Yii::$app->mailer->compose()
                    ->setFrom(['henny.panjaitan3004@gmail.com'=>'Del Data Science Club'])
                    ->setTo($user->email)
                    ->setSubject('Informasi Pemberitahuan Verifikasi Jadi Anggota')
                    ->setHtmlBody('
                        <b>Terima Kasih Telah Melakukan Registrasi Pada Del Data Science Club</b>
                        <br><br>
                        <b>Akun anda telah diverifikasi, Silahkan LOGIN menggunakan username dan password yang anda daftarkan</b>')
                    ->send();

                    if($email){
                        Yii::$app->session->setFlash('success', "Registrasi User Berhasil");
                    }
                    
                }else{
                    $user->delete();
                    Yii::$app->session->setFlash('danger', 'Mohon Maaf, Registrasi User Gagal');
                }

                
            } else {
                Yii::$app->session->setFlash('danger', 'Mohon Maaf, Registrasi User Gagal');
            }

            return $this->redirect(['login']);
        } else {
            return $this->render('register_user', [
                'model' => $model,
            ]);
        }
        
    }

    public function actionViewProfile($id)
    {
        $model = User::findOne($id);
        
        return $this->render('view_profile', [
            'model' => $model,
            'id' => $id,
        ]);
    }


    public function actionUpdateuser($id)
    {
        $model = User::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
             
            $foto_profil = UploadedFile::getInstance($model, 'foto_profil');
            if (!is_null($foto_profil)) {
                $tmp = explode('.', $foto_profil->name);
                $ext_foto = end($tmp);
                //$ext_foto = end((explode(".", $image_foto->name)));
                  // generate a unique file name to prevent duplicate filenames
                $model->foto = Yii::$app->security->generateRandomString().".{$ext_foto}";
                
                // the path to save file, you can set an uploadPath
                // in Yii::$app->params (as used in example below)
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/foto_profil/';
                $path_foto = Yii::$app->params['uploadPath'] . $model->foto;
                
                $foto_profil->saveAs($path_foto);  
                    
            }

            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'User berhasil diupdate');
            } else {
                Yii::$app->session->setFlash('error', 'User gagal diupdate');
            }
            return $this->redirect(['view-profile','id'=>$id]);
        } else {
            
            return $this->renderAjax('_form_profil', [
                'model' => $model,
            ]);
        }
    }

    public function actionUpdatepassword($id)
    {
        $model = User::findOne($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (!empty($model->new_password)) {
                $model->setPassword($model->new_password);
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Password berhasil diupdate');
            } else {
                Yii::$app->session->setFlash('error', 'Password gagal diupdate');
            }
            return $this->redirect(['view-profile','id'=>$id]);
        } else {
            
            return $this->renderAjax('_form_update_password', [
                'model' => $model,
            ]);
        }
    }

}
