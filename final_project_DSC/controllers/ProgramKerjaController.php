<?php

namespace app\controllers;

use Yii;
use app\models\ProgramKerja;
use app\models\ProgramKerjaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Setting;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;
use app\modules\systemxadmin\models\User;
use app\modules\systemxadmin\models\AuthAssignment;
use app\modules\systemxadmin\models\AuthItem;
use app\modules\systemxadmin\models\UserSearch;
/**
 * ProgramStudiController implements the CRUD actions for ProgramKerja model.
 */
class ProgramKerjaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ProgramKerja models.
     * @return mixed
     */
    public function actionIndex()
    {
        $program_kerja = ProgramKerja::find()->all();
        $program_kerja_top10 = ProgramKerja::find()->orderBy(['tanggal_awal'=>SORT_DESC])->limit(5)->all();
        $arrayTahun = ArrayHelper::map(Setting::find()->orderBy(['id_setting' => 'DESC'])->all(),'id_setting','tahun_aktif');

        $events = array();
        foreach ($program_kerja as $key => $value) {
            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $value['id_program_kerja'];
            $Event->title = $value['nama_program_kerja'];
            //$Event->start = date('Y-m-d\TH:i:s\Z',strtotime($time->date_start.' '.$time->time_start));
            //$Event->end = date('Y-m-d\TH:i:s\Z',strtotime($time->date_end.' '.$time->time_end));
            
            $Event->start = $value['tanggal_awal'];
            $Event->end = $value['tanggal_akhir'];
            /*$Event->nonstandard = [
                'field1' => 'Something I want to be included in object #1',
                'field2' => 'Something I want to be included in object #2',
            ];*/
            $events[] = $Event;
        }
        
        $searchModel = new ProgramKerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'events' => $events,
            'program_kerja_top10' => $program_kerja_top10,
            'arrayTahun' => $arrayTahun,
        ]);
    }

    /**
     * Displays a single ProgramKerja model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $model_user = User::find()->where(['id'=>$model->updated_by])->one();

        $user = Yii::$app->user->identity;
        $role = AuthAssignment::find()->where(['user_id'=>$user->id])->one();

        return $this->render('view', [
            'model' => $model,
            'model_user' => $model_user,
            'role' => $role,
        ]);
    }

    /**
     * Creates a new ProgramKerja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ProgramKerja();

        $setting = Setting::find()->where(['active'=>10])->orderBy(['id_setting' => 'DESC'])->one();

        if ($model->load(Yii::$app->request->post())) {
             
            $file = UploadedFile::getInstance($model, 'file');
            if (!is_null($file)) {
                $tmp = explode('.', $file->name);
                $ext_foto = end($tmp);
                //$ext_foto = end((explode(".", $image_foto->name)));
                  // generate a unique file name to prevent duplicate filenames
                $model->file_program_kerja = Yii::$app->security->generateRandomString().".{$ext_foto}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/upload/program_kerja/';
                $path_file = Yii::$app->params['uploadPath'] . $model->file_program_kerja;
                
                $file->saveAs($path_file);  
            }
            $model->id_setting = $setting->id_setting;
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Program Kerja Berhasil Disimpan');
            } else {
                Yii::$app->session->setFlash('danger', 'Program Kerja Gagal Disimpan');
            }

            return $this->redirect(['view-all-program-kerja']);
        } else {
            
            return $this->render('create', [
                'model' => $model,
                
            ]);
        }

    }

    /**
     * Updates an existing ProgramKerja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $setting = Setting::find()->where(['active'=>10])->orderBy(['id_setting' => 'DESC'])->one();

        if ($model->load(Yii::$app->request->post())) {
             
            $file = UploadedFile::getInstance($model, 'file');
            if (!is_null($file)) {
                $tmp = explode('.', $file->name);
                $ext_foto = end($tmp);
                //$ext_foto = end((explode(".", $image_foto->name)));
                  // generate a unique file name to prevent duplicate filenames
                $model->file_program_kerja = Yii::$app->security->generateRandomString().".{$ext_foto}";
                Yii::$app->params['uploadPath'] = Yii::$app->basePath . '/web/upload/program_kerja/';
                $path_file = Yii::$app->params['uploadPath'] . $model->file_program_kerja;
                
                $file->saveAs($path_file);  
            }
            
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Program Kerja Berhasil Diupdate');
            } else {
                Yii::$app->session->setFlash('danger', 'Program Kerja Gagal Diupdate');
            }

            return $this->redirect(['view-all-program-kerja']);
        } else {
            
            return $this->render('update', [
                'model' => $model,
                
            ]);
        }

    }

    /**
     * Deletes an existing ProgramKerja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['view-all-program-kerja']);
    }

    /**
     * Finds the ProgramKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ProgramKerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ProgramKerja::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Fungsi Melihat Semua Data Program Kerja
     * @return mixed
     */
    public function actionViewAllProgramKerja()
    {
        
        $searchModel = new ProgramKerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $user = Yii::$app->user->identity;
        $role = AuthAssignment::find()->where(['user_id'=>$user->id])->one();

        return $this->render('view_all_program_kerja', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'role' => $role,
        ]);
    }
}
