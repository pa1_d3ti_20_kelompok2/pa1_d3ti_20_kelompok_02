/*
SQLyog Ultimate v8.55 
MySQL - 5.5.5-10.1.21-MariaDB : Database - db_final_project
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_final_project` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_final_project`;

/*Table structure for table `adrt` */

DROP TABLE IF EXISTS `adrt`;

CREATE TABLE `adrt` (
  `id_adrt` int(11) NOT NULL AUTO_INCREMENT,
  `deskripsi_adrt` text NOT NULL,
  `file_adrt` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_adrt`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `adrt` */

insert  into `adrt`(`id_adrt`,`deskripsi_adrt`,`file_adrt`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'<p><strong>MUKADIMAH</strong></p>\r\n\r\n<p>Bahwa sesungguhnya pembinaan generasi muda Indonesia agar dapat ikut serta berperan aktif dalam mengisi kemerdekaan Negara Kesatuan Republik Indonesia yang berlandaskan Pancasila dan UUD 1945, adalah sesuai dengan tuntutan hati nurani rakyat yang mencita-citakan terlaksananya kebenaran, keadilan dan kesejahteraan yang diridhoi oleh Tuhan Yang Maha Esa. Sebagai bagian dari generasi muda Indonesia, mahasiswa Institut Teknologi Del dengan didorong oleh rasa tanggung jawab dan pengabdiannya terhadap tanah air, bangsa dan almamaternya, berkewajiban untuk belajar, berkarya dan berjuang sesuai dengan bidang pengabdiannya.</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Mahasiswa Institut Teknologi Del berkewajiban untuk belajar dan berkarya dalam bidang Ilmu Data. Untuk menampung dan mengarahkan kegiatan mahasiswa Institut Teknologi Del dalam usaha memenuhi rasa tanggung jawab dan kewajibannya, maka dibentuklah Klub Ilmu Data Del dengan Anggaran Dasar dan Anggaran Rumah Tangga sebagai berikut.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>ANGGARAN DASAR </strong></p>\r\n\r\n<p><strong>Klub Ilmu Data Del </strong></p>\r\n\r\n<p><strong>BAB I<br />\r\nNAMA, WAKTU, DAN KEDUDUKAN</strong></p>\r\n\r\n<p><strong>Pasal 1</strong></p>\r\n\r\n<p>Nama</p>\r\n\r\n<p>Organisasi ini bernama Unit Kegiatan Mahasiswa (UKM) Klub Ilmu Data Del atau disingkat KIDD yang disebut resmi dalam bahasa Inggrisnya Del Data Science Club.</p>\r\n\r\n<p><strong>Pasal 2</strong></p>\r\n\r\n<p>Waktu</p>\r\n\r\n<p>Klub Ilmu Data Del Institut Teknologi Del diresmikan pada tanggal 02 Februari 2020 di Sitoluama, Laguboti, Kabupaten Tobasamosir, Sumatera Utara.</p>\r\n\r\n<p><strong>Pasal 3</strong></p>\r\n\r\n<p>Kedudukan</p>\r\n\r\n<p>Klub Ilmu Data Del Institut Teknologi Del berkedudukan di seluruh fakultas&nbsp; Institut Teknologi Del.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>BAB II<br />\r\nVISI, MISI DAN TUJUAN</strong></p>\r\n\r\n<p><strong>Pasal 4</strong></p>\r\n\r\n<p>Visi</p>\r\n\r\n<p>&ldquo;Menjadikan Klub Ilmu Data Del sebagai wadah pengembangan kreativitas mahasiswa dan masyarakat dalam bidang ilmu data.</p>\r\n\r\n<p><strong>Pasal 5</strong></p>\r\n\r\n<p>Misi</p>\r\n\r\n<ol>\r\n	<li>Sebagai wadah dalam &nbsp;meningkatkan serta mengembangkan &nbsp;minat dan bakat mahasiswa khususnya anggota klub dalam bidang ilmu data.</li>\r\n	<li>Menyelenggarakan berbagai perlombaan di bidang ilmu data yang bisa diikuti khalayak umum.</li>\r\n</ol>\r\n\r\n<p>&nbsp;3.&nbsp;&nbsp;&nbsp; Menjalin kerjasama antarkomunitas ilmu data dari berbagai universitas maupun komunitas &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; lain.</p>\r\n\r\n<p>4.&nbsp;&nbsp;&nbsp; Menyebarluaskan dan mengimplementasikan peran dan fungsi&nbsp; ilmu data dalam kehidupan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; bermasyarakat.<br />\r\n&nbsp;</p>\r\n\r\n<p><strong>Pasal 6</strong></p>\r\n\r\n<p>Tujuan</p>\r\n\r\n<ol>\r\n	<li>Memacu pengembangan bidang keilmuan khususnya ilmu data di Institut Teknologi Del.</li>\r\n	<li>Sebagai penggerak kemajuan teknologi khususnya ilmu data di Institut Teknologi Del.</li>\r\n	<li>Sebagai penampung minat dan bakat mahasiswa Institut Teknologi Del khususnya di bidang ilmu data .</li>\r\n	<li>Menghasilkan anggota klub yang bisa menguasai ilmu data.</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>BAB III<br />\r\nSUSUNAN DAN KETENTUAN KEPENGURUSAN ORGANISASI</strong></p>\r\n\r\n<p><strong>Pasal 7</strong></p>\r\n\r\n<p>Kepengurusan Induk</p>\r\n\r\n<p>Unsur organisasi induk terdiri dari unsur eksekutif yang diperankan oleh Badan Pengurus Harian Klub Ilmu Data Del dan seluruh anggotanya.</p>\r\n\r\n<p><strong>Pasal 8</strong></p>\r\n\r\n<p>Ketentuan Kepengurusan</p>\r\n\r\n<p>Badan Pengurus Harian Klub Ilmu Data Del, ketua, wakil ketua, sekretaris dan bendahara Klub Ilmu Data Del serta seluruh anggotanya tidak dibenarkan untuk merangkap jabatan.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>BAB IV<br />\r\nKEDAULATAN</strong></p>\r\n\r\n<p><strong>Pasal 9</strong></p>\r\n\r\n<p>Kedaulatan tertinggi berada di tangan seluruh anggota Klub Ilmu Data Del Institut Teknologi Del.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>BAB V<br />\r\nKEUANGAN</strong></p>\r\n\r\n<p><strong>Pasal 10</strong></p>\r\n\r\n<p>Keuangan Klub Ilmu Data Del Institut Teknologi Del diperoleh dari:</p>\r\n\r\n<ol>\r\n	<li>Lembaga Kemahasiswaan / BEM ITD/ FITE ITD.</li>\r\n	<li>Iuran anggota Klub Ilmu Data Del.</li>\r\n	<li>Usaha-usaha lain yang halal dan sah serta tidak bertentangan dengan asas dan tujuan Klub Ilmu Data Del.</li>\r\n	<li>Sumbangan-sumbangan yang tidak mengikat.</li>\r\n</ol>\r\n\r\n<p><strong>BAB VI<br />\r\nLAMBANG, BENDERA DAN JAKET</strong></p>\r\n\r\n<p><strong>Pasal 11</strong></p>\r\n\r\n<p>Lambang</p>\r\n\r\n<p><strong>Pasal 12</strong></p>\r\n\r\n<p>Bendera</p>\r\n\r\n<p><strong>Pasal 13</strong></p>\r\n\r\n<p>Kaos</p>\r\n\r\n<p><strong>BAB VII<br />\r\nKEDUDUKAN ORGANISASI</strong></p>\r\n\r\n<p><strong>Pasal 14</strong></p>\r\n\r\n<p>1. Badan Pengurus Klub Ilmu Data Del berkedudukan di Institut Teknologi Del.</p>\r\n\r\n<p>2. Klub Ilmu Data Del merupakan badan otonom yang berkoordinasi dengan Keluarga</p>\r\n\r\n<p><strong>&nbsp;&nbsp;&nbsp; </strong>Mahasiswa Institut Teknologi Del.</p>\r\n\r\n<p><strong>BAB VIII<br />\r\nPERUBAHAN ANGGARAN DASAR DAN ANGGARAN RUMAH TANGGA</strong></p>\r\n\r\n<p><strong>Pasal 15</strong></p>\r\n\r\n<p>Pelaksanaan</p>\r\n\r\n<ol>\r\n	<li>Perubahan Anggaran Dasar paling banyak dilakukan 1 (satu) kali dalam 1 tahun kepengurusan.</li>\r\n	<li>Perubahan Anggaran Rumah Tangga dilakukan satu atau lebih dalam 1 (satu) tahun kepengurusan disesuaikan dengan kebutuhan perubahan.</li>\r\n</ol>\r\n\r\n<p><strong>Pasal 16</strong></p>\r\n\r\n<p>Ketentuan</p>\r\n\r\n<p>1. Usulan perubahan Anggaran Dasar dan Anggaran Rumah Tangga diatur oleh Badan Pengurus &nbsp;Harian Klub Ilmu Data Del.</p>\r\n\r\n<p>2. Pertimbangan usulan perubahan Anggaran Dasar dan Anggaran Rumah Tangga dapat &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dilakukan oleh Badan Pengurus Harian Klub Ilmu Data Del melalui musyarawah dan dapat disahkan apabila disetujui oleh &nbsp;sekurang-kurangnya 2/3 (dua pertiga) dari seluruh anggota Klub Ilmu Data Del.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>BAB IX<br />\r\nHAL LAIN-LAIN</strong></p>\r\n\r\n<p><strong>Pasal 17</strong></p>\r\n\r\n<p>Anggaran Dasar dan Anggaran Rumah Tangga ini ditetapkan Badan Pengurus Harian Klub Ilmu Data Del dalam rapat paripurna dan diketahui oleh pembina UKM Klub Ilmu Data Del dan seluruh anggota UKM Klub Ilmu Data Del pada tanggal dan tempat yang ditentukan.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Pasal 18</strong></p>\r\n\r\n<p>Dengan ditetapkannya Anggaran Dasar dan Anggaran Rumah Tangga UKM Klub Ilmu Data Del &nbsp;ini, maka Anggaran Dasar dan Anggaran Rumah Tangga UKM Klub Ilmu Data Del yang sebelumnya dinyatakan tidak berlaku.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>ANGGARAN RUMAH TANGGA</strong></p>\r\n\r\n<p><strong>Klub Ilmu Data Del</strong></p>\r\n\r\n<p><strong>BAB I<br />\r\nKEANGGOTAAN</strong></p>\r\n\r\n<p><strong>Pasal 1</strong></p>\r\n\r\n<p>Penerimaan Anggota</p>\r\n\r\n<p>Keikutsertaan dalam keanggotaan Klub Ilmu Data Del adalah mahasiswa yang telah melewati tahap seleksi sesuai dengan mekanisme penerimaan anggota himpunan yang diselenggarakan oleh Badan Pengurus Harian Klub Ilmu Data Del.</p>\r\n\r\n<p><strong>Pasal 2</strong></p>\r\n\r\n<p>Kewajiban Anggota</p>\r\n\r\n<ol>\r\n	<li>Mematuhi semua aturan dalam Anggaran Dasar dan Anggaran Rumah Tangga, keputusan-keputusan BPH dan peraturan-peraturan lain yang telah disahkan.</li>\r\n	<li>Memajukan aktivitas dan usaha-usaha Klub Ilmu Data Del.</li>\r\n	<li>Menjaga nama baik dan kehormatan Klub Ilmu Data Del.</li>\r\n	<li>Menjaga dan memelihara inventaris Klub Ilmu Data Del.</li>\r\n	<li>Membayar iuran anggota sesuai dengan yang ditetapkan dan disahkan oleh Badan Pengurus Harian Klub Ilmu Data Del.</li>\r\n	<li>Uang Pangkal setiap anggota adalah sebesar Rp50.000.</li>\r\n	<li>Bersedia membayar dana tak terduga apabila pelaksanaan program kerja mengalami kekurangan dana.</li>\r\n</ol>\r\n\r\n<p><strong>Pasal 3</strong></p>\r\n\r\n<p>Hak Anggota</p>\r\n\r\n<ol>\r\n	<li>Setiap anggota berhak mengikuti Pemilihan Umum Klub Ilmu Data Del sesuai dengan aturan yang berlaku.</li>\r\n	<li>Setiap anggota berhak menjabat dalam kegiatan dan kepanitiaan yang dibuat oleh mahasiswa Institut Teknologi Del.</li>\r\n	<li>Setiap anggota berhak turut serta dalam kegiatan-kegiatan yang diselenggarakan oleh Institut Teknologi Del.</li>\r\n	<li>Setiap anggota berhak mengajukan pendapat dan saran demi perbaikan dan kemajuan Klub Ilmu Data Del kepada badan pengurus harian.</li>\r\n</ol>\r\n\r\n<p><strong>Pasal 4</strong></p>\r\n\r\n<p>Berakhirnya Keanggotaan</p>\r\n\r\n<ol>\r\n	<li>Keanggotan anggota UKM Klub Ilmu Data Del akan dihapus bila anggota tersebut telah meninggal dunia.</li>\r\n	<li>Menyatakan diri secara tertulis mengundurkan diri dari keanggotaan UKM Klub Ilmu Data Del.</li>\r\n	<li>Melanggar norma dan aturan yang berlaku di UKM Klub Ilmu Data Del melalui pertimbangan badan pengurus harian.</li>\r\n</ol>\r\n\r\n<p><strong>Pasal 5</strong></p>\r\n\r\n<p>Sanksi</p>\r\n\r\n<ol>\r\n	<li>Anggota Del Klub Ilmu Data Del dapat dikenai sanksi apabila melanggar Anggaran Dasar dan Anggaran Rumah Tangga dan atau peraturan-peraturan yang berlaku pada saat itu.</li>\r\n	<li>Sanksi tersebut dapat berupa peringatan ringan, peringatan keras atau pemberhentian dari keanggotaan UKM Klub Ilmu Data Del.</li>\r\n	<li>Keputusan sanksi ditetapkan dalam sidang Majelis Permusyawaratan Anggota Klub Ilmu Data Del.</li>\r\n</ol>\r\n\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>\r\n\r\n<p><strong>Pasal 6</strong></p>\r\n\r\n<p>Kaderisasi</p>\r\n\r\n<ol>\r\n	<li>Dilaksanakan oleh Badan Pengurus Harian dengan persetujuan dari Majelis Permusyawaratan Anggota.</li>\r\n	<li>Mekanisme pelaksanaan dapat berubah sesuai dengan pertimbangan Badan Pengurus Harian.</li>\r\n	<li>Menjunjung tinggi nilai-nilai spiritualitas, moralitas dan intelektualitas.</li>\r\n	<li>Pendaftar wajib mengisi formulir Pendaftaran Keanggotan UKM Del Klub Ilmu Data Del.</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>BAB II<br />\r\nBadan Pengurus Harian Klub Ilmu Data Del ( Sebelumnya )</strong></p>\r\n\r\n<p><strong>Pasal 7</strong></p>\r\n\r\n<p>Badan Pengurus Harian (BPH) adalah majelis tertinggi yang melaksanakan kedaulatan anggota UKM Klub Ilmu Data Del.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Pasal 8</strong></p>\r\n\r\n<p>Keanggotaan</p>\r\n\r\n<ol>\r\n	<li>Anggota Klub Ilmu Data Del adalah mahasiswa yang telah lolos seleksi masuk perekrutan anggota baru.</li>\r\n	<li>Anggota &nbsp;&nbsp; Klub Ilmu Data Del tetap keanggotaannya kecuali mengalami pemberhentian sesuai aturan pemberhentian kepengurusan.</li>\r\n</ol>\r\n\r\n<p><strong>Pasal 9</strong></p>\r\n\r\n<p>Keputusan Badan Pengurus Harian Klub Ilmu Data Del merupakan keputusan tertinggi yang harus dipatuhi oleh seluruh anggota klub Ilmu Data Del.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Pasal 10</strong></p>\r\n\r\n<p>Pemilihan Umum Ketua dan Wakil Ketua Klub Ilmu Data Del</p>\r\n\r\n<ol>\r\n	<li>Pemilihan Umum dilaksanakan pada setiap satu kali periode jabatan(satu tahun jabatan).</li>\r\n	<li>Proses seleksi calon ketua dan wakil ketua Klub Ilmu Data Del mengikuti peraturan/syarat yang ditentukan setelahnya di Klub Ilmu Data Del.</li>\r\n	<li>Mekanisme pemilihan umum ketua dan wakil ketua Klub Ilmu Data Del yang baru ditetapkan oleh Badan Pengurus Harian Klub Ilmu Data Del yang sebelumnya.</li>\r\n</ol>\r\n\r\n<p><strong>BAB III<br />\r\nBADAN PENGURUS HARIAN</strong></p>\r\n\r\n<p><strong>Pasal 11</strong></p>\r\n\r\n<p>Badan Pengurus Harian (BPH) adalah badan pelaksana yang bertanggung jawab kepada dosen pembimbing atas pelaksanaan program kerja Klub Ilmu Data Del.</p>\r\n\r\n<p><strong>Pasal 12</strong></p>\r\n\r\n<p>Keanggotaan Badan Pengurus</p>\r\n\r\n<ol>\r\n	<li>Badan Pengurus Harian terdiri dari ketua,wakil ketua, sekretaris, bendahara.</li>\r\n	<li>Ketua dan wakil ketua Klub Ilmu Data Del dipilih melalui Pemilihan Umum yang diselenggarakan oleh badan pengurus harian sebelumnya dan kemudian diangkat/dilantik oleh Badan Pengurus Harian sebelumnya juga.</li>\r\n	<li>Susunan Badan Pengurus Harian yang dibentuk oleh ketua dan wakil ketua diserahkan selambat-lambatnya dua minggu setelah ketua dan wakil ketua diangkat oleh badan pengurus harian sebelumnya.</li>\r\n	<li>Syarat untuk menduduki jabatan ketua dan wakil ketua &nbsp;Klub Ilmu Data Del minimal sudah 2 (dua) tahun menjalani masa studi.</li>\r\n	<li>Masa jabatan Badan Pengurus Harian Klub Ilmu Data Del adalah satu periode kepengurusan, terhitung sejak dikeluarkannya surat ketetapan badan pengurus harian sebelumnya tentang pengesahan ketua dan wakil ketua terpilih.</li>\r\n</ol>\r\n\r\n<p><strong>Pasal 13</strong></p>\r\n\r\n<p>Kewajiban Badan Pengurus Harian</p>\r\n\r\n<ol>\r\n	<li>Melaksanakan AD/ART KIDD IT Del, Garis Besar Kebijakan (GBK).</li>\r\n	<li>Bertanggung jawab secara hukum terhadap segala masalah yang berkaitan dengan Klub Ilmu Data Del.</li>\r\n	<li>Mengajukan rancangan program kerja dan anggaran pendapatan/belanja Klub Ilmu Data Del dalam suatu Musyawarah Kerja untuk kemudian diajukan kepada dosen pembimbing.</li>\r\n	<li>Mengadakan proses kaderisasi/penerimaan anggota baru minimal 1 (satu) kali dalam satu masa kepengurusan.</li>\r\n</ol>\r\n\r\n<p><strong>Pasal 14</strong></p>\r\n\r\n<p>Hak Badan Pengurus</p>\r\n\r\n<ol>\r\n	<li>Melakukan segala perbuatan hukum untuk dan atas nama Klub Ilmu Data Del.</li>\r\n	<li>Mewakili Klub Ilmu Data Del di dalam dan di luar Institut Teknologi Del.</li>\r\n	<li>Membuat keputusan penting yang membawa sikap dan nama baik Klub Ilmu Data Del, setelah sebelumnya diadakan pertemuan dengan seluruh elemen Klub&nbsp; Ilmu Data Del.</li>\r\n	<li>Pengangkatan dan pemberhentian anggota sesuai dengan mekanisme yang berlaku.</li>\r\n	<li>Mengatur pengelolaan keuangan Klub Ilmu Data Del.</li>\r\n	<li>Mengatur peraturan teknis pelaksanaan program kerja Klub Ilmu Data Del.</li>\r\n	<li>Membentuk dan mengangkat panitia-panitia khusus untuk kegiatan-kegiatan Klub Ilmu Data Del.</li>\r\n	<li>Setiap anggota Badan Pengurus Harian Klub Ilmu Data Del berhak memperoleh pembebasan pembayaran iuran rutin selama masa jabatannya.</li>\r\n</ol>\r\n\r\n<p><strong>Pasal 15</strong></p>\r\n\r\n<p>Musyawarah Kerja</p>\r\n\r\n<ol>\r\n	<li>Musyawarah Kerja adalah rapat yang dilaksanakan pada awal masa kepengurusan untuk mempresentasikan dan menetapkan program kerja Badan Pengurus Harian, yang sifatnya terbuka untuk seluruh anggota Klub Ilmu Data Del.</li>\r\n	<li>Musyawarah Kerja diselenggarakan oleh panitia yang dibentuk dan ditetapkan oleh Badan Pengurus Harian dan penyelenggaraannya wajib diumumkan pada seluruh anggota Klub Ilmu Data Del.</li>\r\n</ol>\r\n\r\n<p><strong>Pasal 16</strong></p>\r\n\r\n<p>Rapat Badan Pengurus Harian</p>\r\n\r\n<ol>\r\n	<li>&nbsp;Rapat Paripurna adalah yang diikuti oleh seluruh anggota Klub Data Del beserta dosen pembimbing untuk membahas hal-hal yang berkaitan dengan pelaksanaan program kerja Klub Ilmu Data Del.</li>\r\n	<li>&nbsp;Rapat Koordinasi adalah rapat terbatas yang hanya diikuti oleh dosen pembina dan badan &nbsp;pengurus harian membahas hal-hal yang berkaitan dengan pelaksanaan program kerja bidang bersangkutan.</li>\r\n	<li>Rapat Umum adalah adalah yang diikuti oleh seluruh anggota Klub Data Del untuk membahas hal-hal yang berkaitan dengan pelaksanaan program kerja Klub Ilmu Data Del.</li>\r\n</ol>\r\n\r\n<p><strong>Pasal 17</strong></p>\r\n\r\n<p>Masa Kepengurusan</p>\r\n\r\n<p>Masa kepengurusan Badan Pengurus Harian (BPH) terhitung 1 periode (2 semester). Pemilihan kepengurusan dilakukan saat semester baru, dimana pemilihannya dilakukan saat semester sedang berjalan dengan tenggang waktu 2 minggu.</p>\r\n\r\n<p><strong>BAB IV<br />\r\nKEUANGAN DAN KEGIATAN</strong></p>\r\n\r\n<p><strong>Pasal 18</strong></p>\r\n\r\n<p>Pengelolaan keuangan sebagai hasil iuran anggota, hasil usaha dari kegiatan-kegiatan yang dilakukan oleh Badan Pengurus Klub Ilmu Data Del, ataupun hasil-hasil usaha lainnya yang sah, halal dan tidak bertentangan asas serta tujuan himpunan, menjadi tanggung jawab Badan Pengurus Klub Ilmu Data Del.</p>\r\n\r\n<p><strong>Pasal 24</strong></p>\r\n\r\n<p>Iuran Anggota</p>\r\n\r\n<ol>\r\n	<li>Iuran anggota sebesar Rp5.000 per bulan.</li>\r\n	<li>Besar biaya ditetapkan dan disahkan oleh Badan Pengurus Harian Klub Ilmu Data Del.</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>Pasal 25</strong></p>\r\n\r\n<p>Kegiatan Klub Ilmu Data Del</p>\r\n\r\n<ol>\r\n	<li>Kegiatan dan proyek adalah usaha-usaha yang sah dan tidak bertentangan dengan asas dan tujuan Klub Ilmu Data Del.</li>\r\n	<li>Setiap dilaksanakan kegiatan dan proyek, dibentuk suatu kepanitiaan yang diatur oleh Badan Pengurus Harian Klub Ilmu Data Del pada saat itu, sesuai dengan mekanisme yang berlaku.</li>\r\n	<li>Kepanitiaan kegiatan dan proyek wajib membuat laporan kepada Badan Pengurus Harian Klub Ilmu Data Del.</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>BAB VI<br />\r\nPEMILIHAN UMUM</strong></p>\r\n\r\n<p><strong>Pasal 26</strong></p>\r\n\r\n<p>Pemilihan Umum adalah pemilihan ketua dan wakil ketua Klub Ilmu Data Del yang diikuti oleh seluruh elemen Klub Ilmu Data Del.</p>\r\n\r\n<p><strong>Pasal 27</strong></p>\r\n\r\n<p>Mekanisme Pemilihan Umum</p>\r\n\r\n<ol>\r\n	<li>Pemilihan Umum diselenggarakan oleh Badan Pengurus Harian Klub Ilmu Data Del sebelum masa kepengurusannya berakhir.</li>\r\n	<li>Aturan pelaksanaan Pemilihan Umum ditetapkan oleh Badan Pengurus Harian Klub Ilmu Data Del.</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>BAB VII<br />\r\nATURAN TAMBAHAN</strong></p>\r\n\r\n<p><strong>Pasal 28</strong></p>\r\n\r\n<ol>\r\n	<li>Segala peraturan yang belum diatur dalam AD/ART KIDD IT Del akan diatur dalam ketetapan-ketetapan &nbsp;Badan Pengurus Harian Klub Ilmu Data Del.</li>\r\n	<li>Peraturan tersebut tidak boleh bertentangan dengan AD/ART KIDD IT Del.</li>\r\n</ol>\r\n','rLMXmbZx23QAWIDAayuUiu8FObgPUIO8.pdf','2020-06-08 20:58:18',397,'2020-06-08 21:31:09',397);

/*Table structure for table `auth_assignment` */

DROP TABLE IF EXISTS `auth_assignment`;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `idx-auth_assignment-user_id` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_assignment` */

insert  into `auth_assignment`(`item_name`,`user_id`,`created_at`) values ('ADMINISTRATOR','1',1589545704),('ADMINISTRATOR','397',1589545714),('ANGGOTA','559',1591608897),('ANGGOTA','560',1591608868),('ANGGOTA','561',1591608839),('ANGGOTA','562',1591608818),('ANGGOTA','569',NULL),('ANGGOTA','571',1591872145),('ANGGOTA','572',1591872193),('ANGGOTA','573',1591872246),('ANGGOTA','574',1591872282),('PEMBINA','567',1591608608),('PENGURUS','552',1591608747),('PESERTA','553',1589562540),('PESERTA','554',1589562548),('PESERTA','555',1589562560),('PESERTA','556',1589562570),('PESERTA','557',1589562577),('PESERTA','558',1589562585),('PESERTA','563',1589638889),('PESERTA','566',NULL),('PESERTA','568',NULL),('PESERTA','570',NULL);

/*Table structure for table `auth_item` */

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item` */

insert  into `auth_item`(`name`,`type`,`description`,`rule_name`,`data`,`created_at`,`updated_at`) values ('/*',2,NULL,NULL,NULL,1589546620,1589546620),('/adrt/*',2,NULL,NULL,NULL,1591871375,1591871375),('/adrt/index',2,NULL,NULL,NULL,1591871186,1591871186),('/adrt/view',2,NULL,NULL,NULL,1591871187,1591871187),('/hasil-voting/online-voting',2,NULL,NULL,NULL,1589618988,1589618988),('/kategori/home-kategori-materi',2,NULL,NULL,NULL,1591871137,1591871137),('/materi/index',2,NULL,NULL,NULL,1591871136,1591871136),('/materi/update',2,NULL,NULL,NULL,1591871385,1591871385),('/materi/view',2,NULL,NULL,NULL,1591871134,1591871134),('/materi/view-materi',2,NULL,NULL,NULL,1591871135,1591871135),('/pengurus/*',2,NULL,NULL,NULL,1591871372,1591871372),('/pengurus/home-daftar-user',2,NULL,NULL,NULL,1591871268,1591871268),('/pengurus/list-user',2,NULL,NULL,NULL,1591871269,1591871269),('/program-kerja/*',2,NULL,NULL,NULL,1591871347,1591871347),('/program-kerja/index',2,NULL,NULL,NULL,1591871155,1591871155),('/program-kerja/view',2,NULL,NULL,NULL,1591871154,1591871154),('/program-kerja/view-all-program-kerja',2,NULL,NULL,NULL,1591871145,1591871145),('/site/index',2,NULL,NULL,NULL,1591871150,1591871150),('/site/login',2,NULL,NULL,NULL,1591871151,1591871151),('/site/logout',2,NULL,NULL,NULL,1591871152,1591871152),('/site/register',2,NULL,NULL,NULL,1591871153,1591871153),('/site/updatepassword',2,NULL,NULL,NULL,1591871353,1591871353),('/site/updateuser',2,NULL,NULL,NULL,1591871709,1591871709),('/site/view-profile',2,NULL,NULL,NULL,1591871731,1591871731),('/submit-tantangan/*',2,NULL,NULL,NULL,1591871361,1591871361),('/submit-tantangan/create',2,NULL,NULL,NULL,1591871175,1591871175),('/submit-tantangan/index',2,NULL,NULL,NULL,1591871157,1591871157),('/submit-tantangan/update',2,NULL,NULL,NULL,1591872426,1591872426),('/submit-tantangan/view',2,NULL,NULL,NULL,1591871159,1591871159),('/systemxadmin/route/*',2,NULL,NULL,NULL,1591871306,1591871306),('/systemxadmin/user/*',2,NULL,NULL,NULL,1591871306,1591871306),('/systemxadmin/user/index',2,NULL,NULL,NULL,1591871340,1591871340),('/systemxadmin/user/resetpassword',2,NULL,NULL,NULL,1591871338,1591871338),('/tantangan/*',2,NULL,NULL,NULL,1591871305,1591871305),('/tantangan/index',2,NULL,NULL,NULL,1591871162,1591871162),('/tantangan/view',2,NULL,NULL,NULL,1591871166,1591871166),('/user/*',2,NULL,NULL,NULL,1591871304,1591871304),('/user/index',2,NULL,NULL,NULL,1591871197,1591871197),('/user/view',2,NULL,NULL,NULL,1591871198,1591871198),('ADMINISTRATOR',1,NULL,NULL,NULL,1589545557,1589545557),('ANGGOTA',1,NULL,NULL,NULL,1591544927,1591544927),('PEMBINA',1,NULL,NULL,NULL,1591544934,1591544934),('PENGURUS',1,NULL,NULL,NULL,1591544941,1591544941),('PESERTA',1,NULL,NULL,NULL,1589545587,1591544894);

/*Table structure for table `auth_item_child` */

DROP TABLE IF EXISTS `auth_item_child`;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item_child` */

insert  into `auth_item_child`(`parent`,`child`) values ('ADMINISTRATOR','/*'),('ANGGOTA','/adrt/index'),('ANGGOTA','/adrt/view'),('ANGGOTA','/kategori/home-kategori-materi'),('ANGGOTA','/materi/index'),('ANGGOTA','/materi/view'),('ANGGOTA','/materi/view-materi'),('ANGGOTA','/pengurus/home-daftar-user'),('ANGGOTA','/pengurus/list-user'),('ANGGOTA','/program-kerja/index'),('ANGGOTA','/program-kerja/view'),('ANGGOTA','/program-kerja/view-all-program-kerja'),('ANGGOTA','/site/index'),('ANGGOTA','/site/login'),('ANGGOTA','/site/logout'),('ANGGOTA','/site/register'),('ANGGOTA','/site/updatepassword'),('ANGGOTA','/site/view-profile'),('ANGGOTA','/submit-tantangan/create'),('ANGGOTA','/submit-tantangan/index'),('ANGGOTA','/submit-tantangan/update'),('ANGGOTA','/submit-tantangan/view'),('ANGGOTA','/tantangan/index'),('ANGGOTA','/tantangan/view'),('PEMBINA','/*'),('PENGURUS','/adrt/*'),('PENGURUS','/kategori/home-kategori-materi'),('PENGURUS','/materi/index'),('PENGURUS','/materi/view'),('PENGURUS','/materi/view-materi'),('PENGURUS','/pengurus/*'),('PENGURUS','/program-kerja/*'),('PENGURUS','/site/index'),('PENGURUS','/site/login'),('PENGURUS','/site/logout'),('PENGURUS','/site/register'),('PENGURUS','/site/updatepassword'),('PENGURUS','/site/view-profile'),('PENGURUS','/submit-tantangan/index'),('PENGURUS','/submit-tantangan/view'),('PENGURUS','/tantangan/index'),('PESERTA','/hasil-voting/online-voting');

/*Table structure for table `auth_rule` */

DROP TABLE IF EXISTS `auth_rule`;

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_rule` */

/*Table structure for table `kategori` */

DROP TABLE IF EXISTS `kategori`;

CREATE TABLE `kategori` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `kategori` */

insert  into `kategori`(`id_kategori`,`nama_kategori`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (1,'PROGRAMMING','2020-06-07 23:59:21',397,'2020-06-08 16:46:55',397),(2,'MATHEMATICS , STATISTIC AND SQL','2020-06-07 23:59:32',1,'2020-06-11 17:22:16',1),(3,'BASIC MACHINE LEARNING','2020-06-07 23:59:40',1,'2020-06-11 17:22:30',1),(4,'NEURAL NETWORK','2020-06-07 23:59:46',1,'2020-06-11 17:22:38',1),(5,'PYTHON','2020-06-07 23:59:52',1,'2020-06-11 17:23:13',1);

/*Table structure for table `materi` */

DROP TABLE IF EXISTS `materi`;

CREATE TABLE `materi` (
  `id_materi` int(11) NOT NULL AUTO_INCREMENT,
  `judul_materi` varchar(255) NOT NULL,
  `deskripsi_materi` varchar(8000) DEFAULT NULL,
  `file_materi` varchar(255) NOT NULL,
  `id_kategori` int(11) NOT NULL,
  `id_setting` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_materi`),
  KEY `kategori_materi` (`id_kategori`),
  KEY `setting_materi` (`id_setting`),
  CONSTRAINT `kategori_materi` FOREIGN KEY (`id_kategori`) REFERENCES `kategori` (`id_kategori`),
  CONSTRAINT `setting_materi` FOREIGN KEY (`id_setting`) REFERENCES `setting` (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

/*Data for the table `materi` */

insert  into `materi`(`id_materi`,`judul_materi`,`deskripsi_materi`,`file_materi`,`id_kategori`,`id_setting`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (13,'PROGRAMMING 1','','RvzCVLVOTfHPSA5heDcECYM7ESLe7d5g.pdf',1,1,'2020-06-11 17:37:42',1,'2020-06-11 17:37:42',1),(14,'PROGRAMMING 2','','HTKo7q5IJwfvNTbzoIOu0mxxB3SIFK5e.pdf',1,1,'2020-06-11 17:38:01',1,'2020-06-11 17:38:01',1),(15,'PROGRAMMING 3','','YLxDf-gSS2kuc-JXwShjGkWVKqdAK3LL.pdf',1,1,'2020-06-11 17:38:21',1,'2020-06-11 17:38:21',1),(16,'PROGRAMMING 4','','EUZJVJMCHE3PlQy-rE-3yXyHb6Y61GDY.pdf',1,1,'2020-06-11 17:38:38',1,'2020-06-11 17:38:38',1),(17,'PROGRAMMING 5','','ADdSbApgK1hRs5xArgykIrCwE8-Vgm1Q.pdf',1,1,'2020-06-11 17:38:56',1,'2020-06-11 17:38:56',1);

/*Table structure for table `program_kerja` */

DROP TABLE IF EXISTS `program_kerja`;

CREATE TABLE `program_kerja` (
  `id_program_kerja` int(11) NOT NULL AUTO_INCREMENT,
  `nama_program_kerja` varchar(255) NOT NULL,
  `deskripsi_program_kerja` text,
  `tanggal_awal` date NOT NULL,
  `tanggal_akhir` date NOT NULL,
  `file_program_kerja` varchar(255) DEFAULT NULL,
  `id_setting` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '10',
  PRIMARY KEY (`id_program_kerja`),
  KEY `setting_program_kerja` (`id_setting`),
  CONSTRAINT `setting_program_kerja` FOREIGN KEY (`id_setting`) REFERENCES `setting` (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=latin1;

/*Data for the table `program_kerja` */

insert  into `program_kerja`(`id_program_kerja`,`nama_program_kerja`,`deskripsi_program_kerja`,`tanggal_awal`,`tanggal_akhir`,`file_program_kerja`,`id_setting`,`created_at`,`created_by`,`updated_at`,`updated_by`,`active`) values (101,'Understanding data science and getting started with python','how to understand data science','2020-01-01','2020-01-30',NULL,1,'2020-06-08 01:13:19',397,'2020-06-08 01:13:19',397,10),(102,'mathematics statistic and sql for data science','how to understand mathematics statistic and sql for data science','2020-02-01','2020-02-28',NULL,1,'2020-06-08 01:13:19',397,'2020-06-08 01:13:19',397,10),(104,'ensemble models and techniques','how to understand ensemble models and techniques','2020-04-01','2020-04-30',NULL,1,'2020-06-08 01:13:19',397,'2020-06-08 01:13:19',397,10),(105,'validation hyperparameter tuning and time series','how to understand validation hyperparameter tuning and time series','2020-05-01','2020-05-30',NULL,1,'2020-06-08 01:13:19',397,'2020-06-08 01:13:19',397,10),(106,'matrix algebra and recommender system','how to understand matrix algebra and recommender system','2020-06-01','2020-06-30',NULL,1,'2020-06-08 01:13:19',397,'2020-06-08 01:13:19',397,10),(107,'getting started with neural network andd deep learning','how to understand neural network','2020-07-01','2020-07-30',NULL,1,'2020-06-08 01:13:19',397,'2020-06-08 01:13:19',397,10),(108,'convolutional neural network','how to understand convolutional neural network','2020-08-01','2020-08-30',NULL,1,'2020-06-08 01:13:19',397,'2020-06-08 01:13:19',397,10),(109,'computer vision projects','how to understand computer vision projects','2020-09-01','2020-09-30',NULL,1,'2020-06-08 01:13:19',397,'2020-06-08 01:13:19',397,10),(110,'Natural language processing','how to understand Natural language processing','2020-10-01','2020-10-30',NULL,1,'2020-06-08 01:13:19',397,'2020-06-08 01:13:19',397,10),(111,'Natural language processing','how to understand Natural language processingg','2020-11-01','2020-11-30',NULL,1,'2020-06-08 01:13:19',397,'2020-06-08 01:13:19',397,10),(112,'Applying for jobs','how to Applying for jobs how to Applying for jobs how to Applying for jobs how to Applying for jobs how to Applying for jobs how to Applying for jobs how to Applying for jobs how to Applying for jobs how to Applying for jobs','2020-12-01','2020-12-30',NULL,1,'2020-06-08 01:13:19',397,'2020-06-09 18:36:35',397,10),(113,'Learn Machine Learning','<p><b style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">Machine learning</b><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">&nbsp;(</span><b style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">ML</b><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">) is the study of computer algorithms that improve automatically through experience.</span><sup class=\"reference\" id=\"cite_ref-1\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; color: rgb(32, 33, 34); font-family: sans-serif;\"><a href=\"https://en.wikipedia.org/wiki/Machine_learning#cite_note-1\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background: none;\">[1]</a></sup><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">&nbsp;It is seen as a subset of&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Artificial_intelligence\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"Artificial intelligence\">artificial intelligence</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">. Machine learning algorithms build a&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Mathematical_model\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"\">mathematical model</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">&nbsp;based on sample data, known as &quot;</span><a class=\"mw-redirect\" href=\"https://en.wikipedia.org/wiki/Training_data\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"Training data\">training data</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">&quot;, in order to make predictions or decisions without being explicitly programmed to do so.</span><sup class=\"reference\" id=\"cite_ref-2\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; color: rgb(32, 33, 34); font-family: sans-serif;\"><a href=\"https://en.wikipedia.org/wiki/Machine_learning#cite_note-2\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background: none;\">[2]</a></sup><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">&nbsp;Machine learning algorithms are used in a wide variety of applications, such as&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Email_filtering\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"Email filtering\">email filtering</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">&nbsp;and&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Computer_vision\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"Computer vision\">computer vision</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">, where it is difficult or infeasible to develop conventional algorithms to perform the needed tasks.</span></p>\r\n\r\n<p><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">Machine learning is closely related to&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Computational_statistics\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"Computational statistics\">computational statistics</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">, which focuses on making predictions using computers. The study of&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Mathematical_optimization\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"Mathematical optimization\">mathematical optimization</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">&nbsp;delivers methods, theory and application domains to the field of machine learning.&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Data_mining\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"Data mining\">Data mining</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">&nbsp;is a related field of study, focusing on&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Exploratory_data_analysis\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"Exploratory data analysis\">exploratory data analysis</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">&nbsp;through&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Unsupervised_learning\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"Unsupervised learning\">unsupervised learning</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">.</span><sup class=\"reference\" id=\"cite_ref-4\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; color: rgb(32, 33, 34); font-family: sans-serif;\"><a href=\"https://en.wikipedia.org/wiki/Machine_learning#cite_note-4\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background: none;\">[4]</a></sup><sup class=\"reference\" id=\"cite_ref-5\" style=\"line-height: 1; unicode-bidi: isolate; white-space: nowrap; font-size: 11.2px; color: rgb(32, 33, 34); font-family: sans-serif;\"><a href=\"https://en.wikipedia.org/wiki/Machine_learning#cite_note-5\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background: none;\">[5]</a></sup><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">&nbsp;In its application across business problems, machine learning is also referred to as&nbsp;</span><a href=\"https://en.wikipedia.org/wiki/Predictive_analytics\" style=\"text-decoration-line: none; color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial; font-family: sans-serif; font-size: 14px;\" title=\"Predictive analytics\">predictive analytics</a><span style=\"color: rgb(32, 33, 34); font-family: sans-serif; font-size: 14px;\">.</span></p>\r\n','2020-07-01','2020-07-08','veYfYaAwhVxuAyBQ-r47-VfYmta1T8VU.pdf',1,'2020-06-09 19:12:27',397,'2020-06-09 19:16:35',397,10);

/*Table structure for table `route` */

DROP TABLE IF EXISTS `route`;

CREATE TABLE `route` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `route` */

insert  into `route`(`name`,`alias`,`type`,`status`) values ('/*','*','',1),('/adrt/*','*','adrt',1),('/adrt/create','create','adrt',1),('/adrt/delete','delete','adrt',1),('/adrt/index','index','adrt',1),('/adrt/update','update','adrt',1),('/adrt/view','view','adrt',1),('/debug/*','*','debug',1),('/debug/default/*','*','debug/default',1),('/debug/default/db-explain','db-explain','debug/default',1),('/debug/default/download-mail','download-mail','debug/default',1),('/debug/default/index','index','debug/default',1),('/debug/default/toolbar','toolbar','debug/default',1),('/debug/default/view','view','debug/default',1),('/debug/user/*','*','debug/user',1),('/debug/user/reset-identity','reset-identity','debug/user',1),('/debug/user/set-identity','set-identity','debug/user',1),('/gii/*','*','gii',1),('/gii/default/*','*','gii/default',1),('/gii/default/action','action','gii/default',1),('/gii/default/diff','diff','gii/default',1),('/gii/default/index','index','gii/default',1),('/gii/default/preview','preview','gii/default',1),('/gii/default/view','view','gii/default',1),('/gridview/*','*','gridview',1),('/gridview/export/*','*','gridview/export',1),('/gridview/export/download','download','gridview/export',1),('/kategori/*','*','kategori',1),('/kategori/create','create','kategori',1),('/kategori/delete','delete','kategori',1),('/kategori/home-kategori-materi','home-kategori-materi','kategori',1),('/kategori/index','index','kategori',1),('/kategori/update','update','kategori',1),('/kategori/view','view','kategori',1),('/materi/*','*','materi',1),('/materi/create','create','materi',1),('/materi/delete','delete','materi',1),('/materi/index','index','materi',1),('/materi/update','update','materi',1),('/materi/view','view','materi',1),('/materi/view-materi','view-materi','materi',1),('/pengurus/*','*','pengurus',1),('/pengurus/home-daftar-user','home-daftar-user','pengurus',1),('/pengurus/list-user','list-user','pengurus',1),('/pengurus/list-verifikasi-pendaftar','list-verifikasi-pendaftar','pengurus',1),('/pengurus/verifikasipendaftar','verifikasipendaftar','pengurus',1),('/program-kerja/*','*','program-kerja',1),('/program-kerja/create','create','program-kerja',1),('/program-kerja/delete','delete','program-kerja',1),('/program-kerja/index','index','program-kerja',1),('/program-kerja/update','update','program-kerja',1),('/program-kerja/view','view','program-kerja',1),('/program-kerja/view-all-program-kerja','view-all-program-kerja','program-kerja',1),('/site/*','*','site',1),('/site/about','about','site',1),('/site/captcha','captcha','site',1),('/site/contact','contact','site',1),('/site/error','error','site',1),('/site/index','index','site',1),('/site/login','login','site',1),('/site/logout','logout','site',1),('/site/register','register','site',1),('/site/updatepassword','updatepassword','site',1),('/site/updateuser','updateuser','site',1),('/site/view-profile','view-profile','site',1),('/submit-tantangan/*','*','submit-tantangan',1),('/submit-tantangan/create','create','submit-tantangan',1),('/submit-tantangan/delete','delete','submit-tantangan',1),('/submit-tantangan/index','index','submit-tantangan',1),('/submit-tantangan/update','update','submit-tantangan',1),('/submit-tantangan/view','view','submit-tantangan',1),('/systemxadmin/*','*','systemxadmin',1),('/systemxadmin/default/*','*','systemxadmin/default',1),('/systemxadmin/default/index','index','systemxadmin/default',1),('/systemxadmin/role/*','*','systemxadmin/role',1),('/systemxadmin/role/create','create','systemxadmin/role',1),('/systemxadmin/role/delete','delete','systemxadmin/role',1),('/systemxadmin/role/get-user-role','get-user-role','systemxadmin/role',1),('/systemxadmin/role/index','index','systemxadmin/role',1),('/systemxadmin/role/index-user-role','index-user-role','systemxadmin/role',1),('/systemxadmin/role/permission','permission','systemxadmin/role',1),('/systemxadmin/role/update','update','systemxadmin/role',1),('/systemxadmin/role/view','view','systemxadmin/role',1),('/systemxadmin/route/*','*','systemxadmin/route',1),('/systemxadmin/route/create','create','systemxadmin/route',1),('/systemxadmin/route/delete','delete','systemxadmin/route',1),('/systemxadmin/route/generate','generate','systemxadmin/route',1),('/systemxadmin/route/index','index','systemxadmin/route',1),('/systemxadmin/route/update','update','systemxadmin/route',1),('/systemxadmin/route/view','view','systemxadmin/route',1),('/systemxadmin/user/*','*','systemxadmin/user',1),('/systemxadmin/user/create','create','systemxadmin/user',1),('/systemxadmin/user/delete','delete','systemxadmin/user',1),('/systemxadmin/user/index','index','systemxadmin/user',1),('/systemxadmin/user/resetpassword','resetpassword','systemxadmin/user',1),('/systemxadmin/user/update','update','systemxadmin/user',1),('/systemxadmin/user/updateuser','updateuser','systemxadmin/user',1),('/systemxadmin/user/view','view','systemxadmin/user',1),('/tantangan/*','*','tantangan',1),('/tantangan/create','create','tantangan',1),('/tantangan/delete','delete','tantangan',1),('/tantangan/index','index','tantangan',1),('/tantangan/rekapitulasi-nilai','rekapitulasi-nilai','tantangan',1),('/tantangan/status-submit','status-submit','tantangan',1),('/tantangan/update','update','tantangan',1),('/tantangan/update-nilai-tantangan','update-nilai-tantangan','tantangan',1),('/tantangan/view','view','tantangan',1),('/user/*','*','user',1),('/user/create','create','user',1),('/user/delete','delete','user',1),('/user/index','index','user',1),('/user/update','update','user',1),('/user/view','view','user',1);

/*Table structure for table `setting` */

DROP TABLE IF EXISTS `setting`;

CREATE TABLE `setting` (
  `id_setting` int(11) NOT NULL AUTO_INCREMENT,
  `nama_aplikasi` varchar(255) NOT NULL,
  `deskripsi_aplikasi` varchar(8000) DEFAULT NULL,
  `tahun_aktif` int(11) NOT NULL,
  `logo_aplikasi` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT '10',
  PRIMARY KEY (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `setting` */

insert  into `setting`(`id_setting`,`nama_aplikasi`,`deskripsi_aplikasi`,`tahun_aktif`,`logo_aplikasi`,`created_at`,`created_by`,`updated_at`,`updated_by`,`active`) values (1,'DSC APPS','Sistem Informasi Del Science CLub',2020,NULL,'2020-06-08 00:29:47',NULL,'2020-06-08 00:29:50',NULL,10);

/*Table structure for table `submit_tantangan` */

DROP TABLE IF EXISTS `submit_tantangan`;

CREATE TABLE `submit_tantangan` (
  `id_tantangan` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `file_submit` varchar(255) NOT NULL,
  `nilai` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tantangan`,`id_user`),
  KEY `user_submit` (`id_user`),
  CONSTRAINT `tantangan_submit` FOREIGN KEY (`id_tantangan`) REFERENCES `tantangan` (`id_tantangan`),
  CONSTRAINT `user_submit` FOREIGN KEY (`id_user`) REFERENCES `t_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `submit_tantangan` */

insert  into `submit_tantangan`(`id_tantangan`,`id_user`,`file_submit`,`nilai`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (4,559,'wfowoiVpAjd5B0Afzlat9R8vtidRmKOl.pdf',NULL,'2020-06-11 17:46:44',559,'2020-06-11 17:47:27',559);

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `auth_key` varchar(255) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `nim` varchar(255) NOT NULL,
  `nama_user` varchar(255) NOT NULL,
  `telp` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `alamat` varchar(500) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `password_reset_token` (`password_reset_token`)
) ENGINE=InnoDB AUTO_INCREMENT=575 DEFAULT CHARSET=latin1;

/*Data for the table `t_user` */

insert  into `t_user`(`id`,`username`,`auth_key`,`password_hash`,`password_reset_token`,`email`,`status`,`nim`,`nama_user`,`telp`,`foto`,`alamat`,`created_at`,`updated_at`) values (1,'admin','vpnmGId3mPT_VsPNGXR3BhffcP4L81Nk','$2y$13$ZzL4Yk46yuoR0bELusWXE.ivAZWux6144VRbW5dfK97/JPPd9ugHq',NULL,'admin@onlinevoting.com',10,'admin','Administrator of DSC','08112322888','fJUAIhrLWhRWSiucWhakI5sQUxCRJfSK.jpg',NULL,1510644852,1591775787),(397,'administrator','','$2y$13$5plhgGsV6UE6ZeAx55SHq.pdhGqpRLEXBB.1ee1J1r1CEhQXNVt0.',NULL,'administrator@gmail.com',10,'administrator','Administrator of DSC',NULL,NULL,NULL,1567596003,1579279763),(552,'henny.panjaitan',NULL,'$2y$13$FqZPJ7ug9zrVtSrRGY2bb.0uYjTIqoDikTHT0J3mFG10pUvBph8r.',NULL,'henny.panjaitan@gmail.com',10,'11319040','Henny Panjaitan',NULL,NULL,NULL,1589545741,1591871038),(553,'user01',NULL,'$2y$13$K90aQtrZx3DC.Dme6WJu9.a2rJvpT4hjmCvUgKBfRlu2N0YY1YyEG',NULL,'user01@gmail.com',10,'','User 01',NULL,NULL,NULL,1589562419,1589562419),(554,'user02',NULL,'$2y$13$L7D6Iv6Gr5T7bGsyDUo6ROKS.vNU9aUOG4P1cOFyX6tXB4WMQghvi',NULL,'user02@gmail.com',10,'','User 02',NULL,NULL,NULL,1589562429,1589562429),(555,'user03',NULL,'$2y$13$sl6bWJZPz.GjtPr0WMhKAei5eNWgZQiwoISRccHZPfbw6QkhHIBTC',NULL,'user03@gmail.com',10,'','User 03',NULL,NULL,NULL,1589562438,1589562438),(556,'user04',NULL,'$2y$13$Pjg7KnVWCZtAyLrcQB8b9ujxzxdRYrVbjPwdNMRVrABEU0mPsCChi',NULL,'user04@gmail.com',10,'','User 04',NULL,NULL,NULL,1589562446,1589562446),(557,'user05',NULL,'$2y$13$yxB/upBadZAd/P4MCG8Gw.LliVEgozNgcg//GA0LWmu3tVligi19G',NULL,'user05@gmail.com',10,'','User 05',NULL,NULL,NULL,1589562454,1589562454),(558,'user06',NULL,'$2y$13$KDN.0RjYfT92Q7bOJSiImukk9vITvIVVPIJVXFBbhUJJY0/JpqZqa',NULL,'user06@gmail.com',10,'','User 06',NULL,NULL,NULL,1589562463,1589562463),(559,'user07',NULL,'$2y$13$VDDgSej5.CaMkvOOxZD8TuFwUM/lVXkDg0YApTspsW/zqZKSH0aTW',NULL,'user07@gmail.com',10,'11320007','User 07',NULL,NULL,NULL,1589562472,1591732772),(560,'user08',NULL,'$2y$13$/Pw1ef.grghMA8JjIHLqKucuHFLR4CaJoSljmyKIvI8aswAv44jEW',NULL,'user08@gmail.com',10,'11320008','User 08',NULL,NULL,NULL,1589562480,1591733307),(561,'user09',NULL,'$2y$13$jw6VCxM4eFP8r9ptrTsNt.ytMaAXC.QJQuKsvquB8GAEqouTiv4wO',NULL,'user09@gmail.com',10,'11319009','User 09',NULL,NULL,NULL,1589562489,1591608833),(562,'user10',NULL,'$2y$13$aSk55g6eDIUPxIyJTy/P4ey8UGTLRTtsiDVgzYMbBajYPLbwz.9Ju',NULL,'user10@gmail.com',10,'11320010','User 10',NULL,NULL,NULL,1589562499,1591608806),(567,'samuel.situmeang',NULL,'$2y$13$LhRGrBL/rmccFbXuT6QR6.0Ma9sOxCXJdv/MKpHozCqs3pOo845pu',NULL,'samuel.situmeang@del.ac.id',10,'19081290821','Samuel Situmeang',NULL,NULL,NULL,1591608592,1591608592),(568,'23518039',NULL,'$2y$13$.E42jLuwiVb1Emu7mjRoN.I4Pp4fMi45jPxSyX.2ZFzj9n2FxJZq.',NULL,'23518039@gmail.com',10,'23518039','Testing Aja',NULL,NULL,NULL,1591777228,1591777228),(571,'michael',NULL,'$2y$13$UlOqg6REs/1xQEqqwqt2uedbooxebG69JpkyH/jwL04.Xpw2Jn5eC',NULL,'michael@gmail.com',10,'11319044','michael',NULL,NULL,NULL,1591872123,1591872135),(572,'jenifer',NULL,'$2y$13$Qtg3XCg/4UgLSO7GtiFcHeWF93qmTOKV1HjM4PiWxC1Akf4Z2VFqu',NULL,'jenifer5@gmail.com',10,'11319045','jenifer ',NULL,NULL,NULL,1591872180,1591872180),(573,'diah',NULL,'$2y$13$xUQ/Pu01cHO2jM2z/0GjkOWoFU3G/HYhr3CewU7rg.9UzrlIDEAXG',NULL,'diah@gmail.com',10,'11319047','diah',NULL,NULL,NULL,1591872239,1591872239),(574,'yolanda',NULL,'$2y$13$mGFMTyGLz6EgkIcoYDC.3ONWUdwsMZw6rmSQH3KwJMrvaHBN3nl5K',NULL,'yolanda@gmail.com',10,'11110058','yolanda',NULL,NULL,NULL,1591872274,1591872274);

/*Table structure for table `tantangan` */

DROP TABLE IF EXISTS `tantangan`;

CREATE TABLE `tantangan` (
  `id_tantangan` int(11) NOT NULL AUTO_INCREMENT,
  `judul_tantangan` varchar(255) NOT NULL,
  `deskripsi_tantangan` text,
  `waktu_buka` datetime NOT NULL,
  `waktu_tutup` datetime NOT NULL,
  `file_tantangan` varchar(255) NOT NULL,
  `id_setting` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tantangan`),
  KEY `setting_tantangan` (`id_setting`),
  CONSTRAINT `setting_tantangan` FOREIGN KEY (`id_setting`) REFERENCES `setting` (`id_setting`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tantangan` */

insert  into `tantangan`(`id_tantangan`,`judul_tantangan`,`deskripsi_tantangan`,`waktu_buka`,`waktu_tutup`,`file_tantangan`,`id_setting`,`created_at`,`created_by`,`updated_at`,`updated_by`) values (3,'LEARN PROGRAMMING 1','','2020-06-11 11:40:00','2020-06-11 19:55:00','H1TK-5VIgGdqwCa43gwtLR2B7FT1vXk_.pdf',1,'2020-06-11 17:39:44',1,'2020-06-11 17:39:44',1),(4,'LEARN PROGRAMMING 2','','2020-06-10 19:00:00','2020-06-11 19:55:00','UGaBQPL4z-iAMZ2ggXO6BUvUze8c2y2B.pdf',1,'2020-06-11 17:40:29',1,'2020-06-11 17:40:29',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
