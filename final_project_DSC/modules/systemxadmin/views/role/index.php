<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\administrator\models\AuthItemSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'DATA ROLE';
?>

<?php
        
    Modal::begin([
        'header' => '<center><h4>DATA ROLE</h4></center>',
        'id' => 'modal-data',
        'size' => 'modal-md',
        'options' => [
              'tabindex' => false, 
          ],
    ]);

    echo '<div id="content-modal-data"></div>';
    Modal::end();
?>

<div class="auth-item-index">
	<div class="row">
      	<div class="col-lg-12 col-md-12 col-sm-6">
        	<div class="box box-solid box-primary">
              	<center><h3><?= Html::encode($this->title) ?></h3></center>
              	
         		<div class="box-body">
         			<p align="right">
						<!-- <?= Html::a('<span class="glyphicon glyphicon-plus"></span> Tambah Role', ['create'], ['class' => 'btn btn-success']) ?> -->

						<?= Html::button('<span class="glyphicon glyphicon-plus"></span> Tambah Role', ['id' => 'modalButton', 'value' => \yii\helpers\Url::to(['create']), 'class' => 'btn btn-success']) ?>
					</p>

					<?= GridView::widget([
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'columns' => [
							['class' => 'yii\grid\SerialColumn'],

							'name',
							/*
							'type',
							'description:ntext',
							'rule_name',
							'data:ntext',
							// 'created_at',
							// 'updated_at',
							*/
							

							[
								'options' => [
                                    'width' => '150px',
                                ],

                                'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {update} {delete}',
                                    'buttons' => [
                                        'view' => function($url, $model) {
                                            return Html::a('<span class="fa fa-bars"></span>', $url, [
                                                        'class' => 'btn btn-info',
                                                        'title' => 'VIEW ROLE & MANAJAMEN ROUTE',
                                                       
                                                    ]);
                                        },

                                        'update' => function($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                                        'class' => 'btn btn-warning',
                                                        'title' => 'EDIT DATA ROLE',
                                                        'id' => 'modalButton',
                                                        'onclick' => "
                                          $.ajax({
                                            url: '$url',
                                            success:function(data){
                                              $('#title').html('Detail Organisasi');
                                              $('#content-modal-data').html(data);
                                              $('#modal-data').modal('show');
                                            },
                                            error:function(xhr, ajaxOptions, throwError){
                                              alert(xhr.responseText);
                                            }
                                          })
                                          ",
                                                    ]);
                                        },

                                        'delete' => function ($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                                        'class' => 'btn btn-danger',
                                                        'title' => 'Hapus',
                                                        'data-toggle' => 'tooltip',
                                                        'data-method' => 'post',
                                                        'data-confirm' => 'Apakah anda yakin ingin menghapus data ini?'
                                                    ]);
                                        },

                                        
                                    ]
							],
						],
					]); ?>
         		</div>
         	</div>
        </div>
    </div>
</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#modalButton').click(function () {
            $('#modal-data').modal('show')
                .find('#content-modal-data')
                .load($(this).attr('value'));
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>