<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\User */

$this->title = '';

?>
<div class="user-view">

    

    <?php $form = ActiveForm::begin([]); ?>
    <?php
        echo $form->field($authAssignment, 'item_name')->widget(Select2::classname(), [
          'data' => $authItems,
          'options' => [
            'placeholder' => 'Role ...',
          ],
          'pluginOptions' => [
            'allowClear' => true,
            'multiple' => true,
          ],
        ])->label('Role'); 
    ?>

    <div class="form-group">
        <center>
            <?= Html::submitButton('Update Role User', [
            'class' => $authAssignment->isNewRecord ? 'btn btn-success' : 'btn btn-primary',
            //'data-confirm'=>"Apakah anda yakin akan menyimpan data ini?",
        ]) ?>
        </center>
        
    </div>
    <?php ActiveForm::end(); ?>
</div>
