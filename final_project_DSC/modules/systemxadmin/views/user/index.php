<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\administrator\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'USER PROFILE';
//$this->params['breadcrumbs'][] = $this->title;
?>

<?php
        
    Modal::begin([
        'header' => '<center><h4>DATA USER</h4></center>',
        'id' => 'modal-data',
        'size' => 'modal-lg',
        'options' => [
              'tabindex' => false, 
          ],
    ]);

    echo '<div id="content-modal-data"></div>';
    Modal::end();
?>

<div class="user-index">
	<div class="row">
      	<div class="col-lg-12 col-md-12 col-sm-6">
        	<div class="box box-solid box-primary">
              <center><h3>DATA USER</h3></center>
         		<div class="box-body">
         			<p align="right">
                       <?= Html::button('<span class="fa fa-user-plus"></span> TAMBAH USER', ['id' => 'modalButton', 'value' => \yii\helpers\Url::to(['create']), 'class' => 'btn btn-success']) ?>
                    </p>
                    <?php Pjax::begin(); ?>

                    <?= GridView::widget([
						'dataProvider' => $dataProvider,
						'filterModel' => $searchModel,
						'columns' => [
							['class' => 'yii\grid\SerialColumn'],

							'username',
							'nim',
              'nama_user',
              'email',
							[
								'attribute' => 'roles',
								'format' => 'raw',
								'value' => function ($data) {
									$roles = [];
									foreach ($data->roles as $role) {
										$roles[] = $role->item_name;
									}
									return Html::a(implode(', ', $roles), ['view', 'id' => $data->id]);
								}
							],
							[
								'attribute' => 'status',
								'format' => 'raw',
								'options' => [
									'width' => '80px',
								],
								'value' => function ($data) {
									if ($data->status == 10)
										return "<span class='label label-primary'>" . 'Active' . "</span>";
									else
										return "<span class='label label-danger'>" . 'Not Active' . "</span>";
								}
							],
							
							/*[
								'attribute' => 'created_at',
								'format' => ['date', 'php:d M Y H:i:s'],
								'options' => [
									'width' => '120px',
								],
							],
							[
								'attribute' => 'updated_at',
								'format' => ['date', 'php:d M Y H:i:s'],
								'options' => [
									'width' => '120px',
								],
							],*/
							[
								'options' => [
                                    'width' => '150px',
                                ],

                                'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {update} {resetpassword}',
                                    'buttons' => [
                                        'view' => function($url, $model) {
                                            return $model->status == 10 ? Html::a('<span class="fa fa-user"></span>', '#', [
                                                        'class' => 'btn btn-info',
                                                        'title' => 'TAMBAH /UBAH ROLE USER',
                                                        'onclick' => "
                                          $.ajax({
                                            url: '$url',
                                            success:function(data){
                                              $('#title').html('Detail Organisasi');
                                              $('#content-modal-data').html(data);
                                              $('#modal-data').modal('show');
                                            },
                                            error:function(xhr, ajaxOptions, throwError){
                                              alert(xhr.responseText);
                                            }
                                          })
                                          ",
                                                    ]) : '';
                                        },

                                        'update' => function($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                                        'class' => 'btn btn-warning',
                                                        'title' => 'EDIT DATA USER',
                                                        'id' => 'modalButton',
                                                        'onclick' => "
                                          $.ajax({
                                            url: '$url',
                                            success:function(data){
                                              $('#title').html('Detail Organisasi');
                                              $('#content-modal-data').html(data);
                                              $('#modal-data').modal('show');
                                            },
                                            error:function(xhr, ajaxOptions, throwError){
                                              alert(xhr.responseText);
                                            }
                                          })
                                          ",
                                                    ]);
                                        },
                                        /*'delete' => function ($url, $model) {
                                            return $model->status == 10 ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                                        'class' => 'btn btn-danger',
                                                        'title' => 'Hapus',
                                                        'data-toggle' => 'tooltip',
                                                        'data-method' => 'post',
                                                        'data-confirm' => 'Apakah anda yakin ingin menghapus data ini?'
                                                    ]) : '';
                                        },*/

                                        'resetpassword' => function ($url, $model) {
                                            return $model->status == 10 ? Html::a('<span class="fa fa-share-square-o"></span>', $url, [
                                                        'class' => 'btn btn-danger',
                                                        'title' => 'RESET PASSWORD',
                                                        'data-toggle' => 'tooltip',
                                                        'data-method' => 'post',
                                                        'data-confirm' => 'Apakah anda yakin ingin mereset password?'
                                                    ]) : '';
                                        },
                                    ]
							],
						],
					]); ?>

					<?php Pjax::end(); ?>
          		</div>
          	</div>
      	</div>
  	</div>
</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#modalButton').click(function () {
            $('#modal-data').modal('show')
                .find('#content-modal-data')
                .load($(this).attr('value'));
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>