<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Update Data User dan Password';
?>


<div class="user-form">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-6">
	    	<div class="box box-solid box-success">
	     		<div class="box-body">
	     			<center>
	     				<h3 class="text-success">Form Update Data User & Update Password User</h3>
	     			</center>
	     			<?php $form = ActiveForm::begin(); ?>

					<?= $form->field($model, 'username')->textInput(['maxlength' => true,'disabled' => true]) ?>

					<?= $form->field($model, 'inisial_user')->textInput(['maxlength' => true,'disabled' => true]) ?>

					<?= $form->field($model, 'nama_user')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'telp')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'status')->hiddenInput(['value'=> 10])->label(false);?>

					<?php if (!$model->isNewRecord) { ?>
						<div class="callout callout-info">
							<center> <b class="text-danger">Jika Password Anda masih menggunakan <u class="text-warning">
							"Password Default"</u>, Silahkan Ubah Password Anda Sesuai Keinginan Anda </b></center>
							<p align="right">
								(<i>Biarkan Kosong, Jika Anda Tidak Mengubah Password</i>)
							</p>
							<div class="ui divider"></div>
							<?= $form->field($model, 'new_password')->passwordInput(['id'=>'new_password']) ?>
							<?= $form->field($model, 'repeat_password')->passwordInput(['id'=>'repeat_password']) ?>
							<?= $form->field($model, 'old_password')->passwordInput(['id'=>'old_password']) ?>

							<?= Html::checkbox('reveal-password', false, ['id' => 'reveal-password']) ?> <?= Html::label('Show password', 'reveal-password') ?>
						</div>

						
					<?php } ?>
					<div class="form-group">
						<center>
							<?= Html::submitButton($model->isNewRecord ? 'Tambah Data User' : 'Update Data User', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
						</center>
					</div>

					<?php ActiveForm::end(); ?>
	     		</div>
	     	</div>
	     </div>
     </div>
</div>


<?php
 
$script = <<< JS
    $(function () {
        $('#reveal-password').change(function () {
            $('#new_password').attr('type',this.checked?'text':'password');
            $('#repeat_password').attr('type',this.checked?'text':'password');
            $('#old_password').attr('type',this.checked?'text':'password');
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>