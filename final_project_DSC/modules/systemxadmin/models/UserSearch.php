<?php

namespace app\modules\systemxadmin\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\systemxadmin\models\User;

/**
 * UserSearch represents the model behind the search form about `hscstudio\mimin\models\User`.
 */
class UserSearch extends User
{
	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id', 'status', 'created_at', 'updated_at'], 'integer'],
			[['username', 'nim','nama_user','telp','auth_key', 'password_hash', 'password_reset_token', 'email'], 'safe'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = User::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
                'defaultOrder' => [
                    
                    'updated_at' => SORT_DESC,
                    
                ]
            ],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'status' => $this->status,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
		]);

		$query->andFilterWhere(['like', 'username', $this->username])
			->andFilterWhere(['like', 'nim', $this->auth_key])
			->andFilterWhere(['like', 'nama_user', $this->password_hash])
			->andFilterWhere(['like', 'telp', $this->password_reset_token])
			->andFilterWhere(['like', 'email', $this->email]);

		return $dataProvider;
	}

	public function searchAnggota($params)
	{
		$query = User::find()->join('JOIN','auth_assignment','auth_assignment.user_id = t_user.id')->where(['auth_assignment.item_name'=>'ANGGOTA']);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
                'pageSize' => 5,
            ],
			'sort' => [
                'defaultOrder' => [
                    'nim' => SORT_ASC,
                    'updated_at' => SORT_DESC,
                    
                ]
            ],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'status' => $this->status,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
		]);

		$query->andFilterWhere(['like', 'username', $this->username])
			->andFilterWhere(['like', 'nim', $this->auth_key])
			->andFilterWhere(['like', 'nama_user', $this->password_hash])
			->andFilterWhere(['like', 'telp', $this->password_reset_token])
			->andFilterWhere(['like', 'email', $this->email]);

		return $dataProvider;
	}

	public function searchListUser($model,$params)
	{
		$query = $model;

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
                'pageSize' => 50,
            ],
			'sort' => [
                'defaultOrder' => [
                    'nim' => SORT_ASC,
                    'updated_at' => SORT_DESC,
                    
                ]
            ],
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'status' => $this->status,
			'created_at' => $this->created_at,
			'updated_at' => $this->updated_at,
		]);

		$query->andFilterWhere(['like', 'username', $this->username])
			->andFilterWhere(['like', 'nim', $this->auth_key])
			->andFilterWhere(['like', 'nama_user', $this->password_hash])
			->andFilterWhere(['like', 'telp', $this->password_reset_token])
			->andFilterWhere(['like', 'email', $this->email]);

		return $dataProvider;
	}
}
