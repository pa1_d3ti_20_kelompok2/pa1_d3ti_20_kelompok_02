<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LIST USER';

//$this->params['breadcrumbs'][] = $this->title;
?>



<div class="kategori-index">
    <div class="row">
        

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-solid box-success">
                <div class="box-header with-border">
                    <center>
                      DAFTAR PESERTA YANG MENDAFTAR PADA DEL DATA SCIENCE CLUB
                    </center>
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'nim',
                            'nama_user',
                            'email',
                            'telp',
                            [
                                
                                'header'=> 'VERIFIKASI PENDAFTAR',
                                'class' => 'yii\grid\ActionColumn',
                                    'template' => '{verifikasipendaftar}',
                                    'buttons' => [
                                        'verifikasipendaftar' => function ($url, $model) {
                                            return  Html::a('<span class="fa fa-send"></span> Veririfikasi PENDAFTAR Jadi ANGGOTA', $url, [
                                                        'class' => 'btn btn-primary',
                                                        'title' => 'VERIFIKASI USER PENDAFTAR',
                                                        'data-toggle' => 'tooltip',
                                                        'data-confirm' => 'Apakah anda yakin ingin verifikasi pendaftar ini?'
                                                    ]);
                                        },


                                        
                                    ]
                            ],
                            
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>
