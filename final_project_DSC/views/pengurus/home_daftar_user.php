<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\Materi */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'DAFTAR ROLE';

?>

<div class="home-daftar-user">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <center><h3>DAFTAR ROLE USER</h3></center>
                    
                </div>
                
                <div class="box-body">
                   
                        <?php
                            $i = 0;
                            foreach ($role as $key => $value) {
                                if ($i == 0) {
                                    $box = "small-box bg-green";
                                }else if($i == 1){
                                    $box = "small-box bg-blue";
                                }else if($i == 2){
                                    $box = "small-box bg-yellow";
                                }else{
                                    $box = "small-box bg-red";
                                }
                        ?>      
                                <div class="col-lg-6 col-md-6 col-xs-6">
                                    <div class="<?=$box?>">
                                        <div class="inner">
                                          <h3><?=$value['name']?></h3>

                                          <p>
                                            <b class="text text-default">
                                                DEL SCIENCE CLUB
                                            </b>
                                          </p>
                                        </div>
                                        <div class="icon">
                                          <i class="fa fa-vcard"></i>
                                        </div>
                                        <!-- <a href="#" class="small-box-footer">LIHAT DAFTAR<i class="fa fa-arrow-circle-right"></i></a> -->
                                        <?= Html::a('<span class="fa fa-arrow-circle-right"></span> LIHAT DAFTAR '.$value['name'], ['list-user', 'role' => $value['name']], ['class' => 'small-box-footer']);?>
                                    </div>
                                </div>

                                
                        <?php
                                $i++;
                            }   
                        ?>
                        
                   
                </div>
            </div>
        </div>
    </div>
    

</div>
