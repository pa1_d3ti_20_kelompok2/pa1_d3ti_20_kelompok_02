<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'LIST USER';

//$this->params['breadcrumbs'][] = $this->title;
?>



<div class="kategori-index">
    <div class="row">
        

        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-solid box-default">
                <div class="box-header with-border">
                    <center>
                      DAFTAR <b class="text-danger"><?= $model_role->name;?></b> DEL DATA SCIENCE CLUB
                    </center>
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'nim',
                            'nama_user',
                            'email',
                            'telp'
                            
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>
