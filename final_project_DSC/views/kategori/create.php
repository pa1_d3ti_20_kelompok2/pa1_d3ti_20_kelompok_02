<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kategori */

$this->title = 'Create Kategori';

?>
<div class="kategori-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
