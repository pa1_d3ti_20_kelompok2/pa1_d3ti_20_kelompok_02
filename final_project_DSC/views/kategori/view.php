<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kategori */

$this->title = 'View Kategori';

?>
<div class="kategori-view">
   
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            
            'nama_kategori',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
