<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Kategori */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kategori-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama_kategori')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <CENTER><?= Html::submitButton('SIMPAN DATA', ['class' => 'btn btn-success']) ?></CENTER>
    </div>

    <?php ActiveForm::end(); ?>

</div>
