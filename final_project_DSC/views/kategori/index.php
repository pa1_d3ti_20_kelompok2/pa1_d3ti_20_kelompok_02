<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Kategori';

?>

<?php
        
    Modal::begin([
        'header' => '<center><h4>DATA KATEGORI</h4></center>',
        'id' => 'modal-data',
        'size' => 'modal-md',
        'options' => [
              'tabindex' => false, 
          ],
    ]);

    echo '<div id="content-modal-data"></div>';
    Modal::end();
?>

<div class="kategori-index">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <center>
                      <h3 class="text-success">DATA KATEGORI MATERI</h3>
                    </center>
                    <p align="right">
                        <?= Html::button('<span class="glyphicon glyphicon-plus"></span> TAMBAH DATA KATEGORI', ['id' => 'modalButton', 'value' => \yii\helpers\Url::to(['create']), 'class' => 'btn btn-success']) ?>
                    </p>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            
                            'nama_kategori',
                            
                            [
                                'options' => [
                                    'width' => '150px',
                                ],
                                'header' => 'AKSI',

                                'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {update} {delete}',
                                    'buttons' => [
                                        'view' => function($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                                                        'class' => 'btn btn-info',
                                                        'title' => 'VIEW DATA',
                                                        'onclick' => "
                                          $.ajax({
                                            url: '$url',
                                            success:function(data){
                                              $('#title').html('Detail Organisasi');
                                              $('#content-modal-data').html(data);
                                              $('#modal-data').modal('show');
                                            },
                                            error:function(xhr, ajaxOptions, throwError){
                                              alert(xhr.responseText);
                                            }
                                          })
                                          ",
                                                    ]);
                                        },

                                        'update' => function($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                                        'class' => 'btn btn-warning',
                                                        'title' => 'EDIT DATA',
                                                        'id' => 'modalButton',
                                                        'onclick' => "
                                          $.ajax({
                                            url: '$url',
                                            success:function(data){
                                              $('#title').html('Detail Organisasi');
                                              $('#content-modal-data').html(data);
                                              $('#modal-data').modal('show');
                                            },
                                            error:function(xhr, ajaxOptions, throwError){
                                              alert(xhr.responseText);
                                            }
                                          })
                                          ",
                                                    ]);
                                        },
                                        'delete' => function ($url, $model) {
                                            return  Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                                        'class' => 'btn btn-danger',
                                                        'title' => 'HAPUS DATA',
                                                        'data-toggle' => 'tooltip',
                                                        'data-method' => 'post',
                                                        'data-confirm' => 'Apakah anda yakin ingin menghapus data ini?'
                                                    ]);
                                        },
                                    ]
                            ],

                            [
                                'options' => [
                                    'width' => '150px',
                                ],
                                'header' => 'MATERI',
                                
                                'format' =>'raw',
                                'value' => function ($model){
                                    if ($model->id_kategori!=NULL) {
                                        return Html::a('<span class="glyphicon glyphicon-list-alt"></span> DATA MATERI',Url::to(['materi/view-materi','id_kategori'=>$model->id_kategori]),['data-pjax' => 0, 'class' => 'btn btn-default btn-sm',
                                                        'title' => 'LIHAT DATA MATERI',]);
                                    }else{
                                        return "-";
                                    }
                                }

                                
                            ],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>

</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#modalButton').click(function () {
            $('#modal-data').modal('show')
                .find('#content-modal-data')
                .load($(this).attr('value'));
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>