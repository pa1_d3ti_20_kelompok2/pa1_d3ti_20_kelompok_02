<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Kategori';

?>

<?php
        
    Modal::begin([
        'header' => '<center><h4>DATA KATEGORI</h4></center>',
        'id' => 'modal-data',
        'size' => 'modal-md',
        'options' => [
              'tabindex' => false, 
          ],
    ]);

    echo '<div id="content-modal-data"></div>';
    Modal::end();
?>

<div class="kategori-index">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <center>
                      <h3 class="text-success">DATA KATEGORI MATERI</h3>
                    </center>
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            //['class' => 'yii\grid\SerialColumn'],

                            
                           /* 'nama_kategori',*/
                            

                            [
                                
                                'header' => 'MATERI',
                                'attribute' => 'nama_kategori',
                                'format' =>'raw',
                                'value' => function ($model){
                                    if ($model->id_kategori!=NULL) {
                                        return Html::a('<span class="fa fa-folder-open fa-2x"></span> MATERI '.$model->nama_kategori.'',Url::to(['materi/view-materi','id_kategori' => $model->id_kategori]),['data-pjax' => 0, 'class' => 'btn btn-default',
                                                        'title' => 'LIHAT DATA MATERI',]);
                                    }else{
                                        return "-";
                                    }
                                }

                                
                            ],
                        ],
                    ]); ?>

                </div>
            </div>
        </div>
    </div>

</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#modalButton').click(function () {
            $('#modal-data').modal('show')
                .find('#content-modal-data')
                .load($(this).attr('value'));
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>