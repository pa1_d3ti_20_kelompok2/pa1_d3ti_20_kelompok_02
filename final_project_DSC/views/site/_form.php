<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

	<?php $form = ActiveForm::begin(); ?>
	<?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'nama_user')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

	<?= $form->field($model, 'password_hash')->passwordInput(['id'=>'new_password']) ?>
		
	<?= Html::checkbox('reveal-password', false, ['id' => 'reveal-password']) ?> <?= Html::label('Show password', 'reveal-password') ?>
	<div class="text-danger">
        * Silahkan Isi Data Pengguna Dengan Baik dan Tidak Boleh Kosong.
    </div>
    <br>
	<div class="form-group">
		<center><?= Html::submitButton($model->isNewRecord ? 'DAFTARKAN' : 'DAFTARKAN', ['class' => $model->isNewRecord ? 'btn btn-success btn-lg' : 'btn btn-primary']) ?></center>
	</div>

	<?php ActiveForm::end(); ?>

</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#reveal-password').change(function () {
            $('#new_password').attr('type',this.checked?'text':'password');
            //$('#repeat_password').attr('type',this.checked?'text':'password');
            //$('#old_password').attr('type',this.checked?'text':'password');
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>