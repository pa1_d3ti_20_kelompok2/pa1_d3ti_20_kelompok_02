<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\User */

$this->title = 'DAFTAR PENGGUNA';
/*$this->params['breadcrumbs'][] = $this->title;*/
?>
<div class="user-create">
	<div class="row">
		<br><br>
		<div class="col-lg-3 col-md-3 col-sm-3">
			
		</div>
		<div class="col-lg-6 col-md-6 col-sm-6">
			<div class="box box-success">
                <div class="box-header with-border">
                    <center>
                      <center><h2>FORM DAFTAR PENGGUNA BARU</h2></center>
                    </center>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                	<?= $this->render('_form', [
				        'model' => $model,
				    ]) ?>
                </div>
            </div>
		</div>
	</div>
    

</div>
