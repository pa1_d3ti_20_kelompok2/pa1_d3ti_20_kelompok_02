<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Update Data User';
?>


<div class="user-form">
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-6">
	    	<div class="box box-solid box-success">
	     		<div class="box-body">
	     			
	     			<?php $form = ActiveForm::begin(); ?>

					<?= $form->field($model, 'username')->textInput(['maxlength' => true,'disabled' => true]) ?>

					<?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'nama_user')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'telp')->textInput(['maxlength' => true]) ?>

					<?= $form->field($model, 'status')->hiddenInput(['value'=> 10])->label(false);?>

					<?= $form->field($model, 'foto_profil')->widget(FileInput::classname(), 
                        [
                            'options' => ['accept' => 'image/*'],
                            'pluginOptions'=>[
                                'allowedFileExtensions'=>['jpg','jpeg','png'],
                                'dropZoneEnabled'=> false,
                                'browseLabel' =>  'Upload Foto Profil'
                            ],
                        ]) 
                    ?>    
					
					<div class="form-group">
						<center>
							<?= Html::submitButton($model->isNewRecord ? 'Update Data User' : 'Update Data User', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-success']) ?>
						</center>
					</div>

					<?php ActiveForm::end(); ?>
	     		</div>
	     	</div>
	     </div>
     </div>
</div>
