<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Tantangan */

$this->title = 'Home';


?>
<div class="tantangan-view">
    <div class="row">
    	<div class="col-md-12 col-sm-12 col-xs-12">
          <div class="info-box">
            <!-- <span class="info-box-icon bg-aqua"></i></span> -->

            <div class="info-box-content">
   			<h1 class="text-purple">
   			 <?php
           	 $urlImage = Yii::getAlias('@web').'/images/del.jpeg';
       		 ?>    
        	       <img src="<?= $urlImage;?>" alt="Logo Image" style="height: 10%;width: 10%;"/>
   					&nbsp &nbsp &nbsp &nbsp &nbsp<b> WELCOME TO DEL DATA SCIENCE CLUB</b> &nbsp &nbsp &nbsp &nbsp
              <?php
           	 $urlImage = Yii::getAlias('@web').'/images/logo_dsc.png';
       		 ?>    
        	<img src="<?= $urlImage;?>" alt="Logo Image" style="height: 12%;width: 12%;"/>
       		 </h1>

              <span class="info-box-number"></span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <div class="col-lg-6 col-md-12 col-sm-12">
        	
            <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                    <span class="bg-green">
                      <h4>VISI MISI DEL DATA SCIENCE CLUB</h4>
                    </span>
                    
                </li>
               
                <li>
                    <!-- timeline icon -->
                    <i class="fa fa-area-chart bg-red"></i>
                    <div class="timeline-item">
                        <span class="time">
                        </span>

                        <h3 class="timeline-header">
                            <b class="text-primary">VISI </b>
                        </h3>

                        <div class="timeline-body">
                         
			            Menjadikan Klub Ilmu Data Del sebagai wadah pengembangan kreativitas mahasiswa dan masyarakat dalam bidang ilmu data<br>
                    </div>
                </li>
                
                <li>
                
                    <i class="fa fa-area-chart bg-yellow"></i>
                    <div class="timeline-item">
                       	<h3 class="timeline-header">
          					 <b class="text-primary">MISI </b>
                        </h3>

                        <div class="timeline-body">
                         

                       1. Sebagai wadah dalam meningkatkan serta mengembangkan minat dan bakat mahasiswa khususnya anggota klub dalam bidang ilmu data.<br>
			            2. Menyelenggarakan berbagai perlombaan di bidang ilmu data yang bisa diikuti khalayak umum.<br>
			            3. Menjalin kerjasama antarkomunikasi ilmu data dari berbagai universitas maupun komunitas lain.<br>
			            4. Menyebarluaskan dan mengimplementasikan peran dan fungsi ilmu data dalam kehidupan bermasyarakat.
                        </div>

                    </div>
                </li>

            </ul>
        </div>

         <div class="col-lg-6 col-md-12 col-sm-12">
        	
            <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                    <span class="bg-purple">
                      <h4>STRUKTUR ORGANISASI</h4>
                    </span>
                    
                </li>
               
                <li>
                    <!-- timeline icon -->
                    <i class="fa fa-user bg-teal"></i>
                    <div class="timeline-item">
                        <span class="time">
                        </span>

                        <h3 class="timeline-header">
                            <b class="text-yellow">PEMBINA </b>
                        </h3>

                        <div class="timeline-body">
                         <b class="text-primary">Samuel Indra Gunawan Situmeang, S.Ti.,M.Sc.</b>
			            <br>
                    </div>
                </li>
                
                <li>
                
                    <i class="fa fa-users bg-maroon"></i>
                    <div class="timeline-item">
                       	<h3 class="timeline-header">
          					 <b class="text-yellow">PENGURUS </b>
                        </h3>


                        <div class="timeline-body">
                   		
                    	<b class="text-primary"> 
                        KETUA     : Irvandy Sihombing<br>
                   		WAKIL     : Unedo Manalu<br>
                   		BENDAHARA : Christopher<br>
                   		SEKRETARIS: Agnes <br>
                    </b>

                   		

                        </div>

                    </div>
                </li>

            </ul>
        </div>
        
        
    </div>
        </div>
    </div>



</div>




