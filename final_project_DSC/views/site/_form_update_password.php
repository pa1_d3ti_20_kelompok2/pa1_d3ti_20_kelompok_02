<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\SwitchInput;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\modules\administrator\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

	<?php $form = ActiveForm::begin(); ?>

	<?php if (!$model->isNewRecord) { ?>
		<div class="ui divider"></div>
		<?= $form->field($model, 'old_password')->passwordInput(['id'=>'old_password']) ?>
		<?= $form->field($model, 'new_password')->passwordInput(['id'=>'new_password']) ?>
		<?= $form->field($model, 'repeat_password')->passwordInput(['id'=>'repeat_password']) ?>
		

		<?= Html::checkbox('reveal-password', false, ['id' => 'reveal-password']) ?> <?= Html::label('Show password', 'reveal-password') ?>

	<?php } ?>
	<div class="form-group">
		<center>
			<?= Html::submitButton($model->isNewRecord ? 'SIMPAN PASSWORD BARU' : 'SIMPAN PASSWORD BARU', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
		</center>
	</div>

	<?php ActiveForm::end(); ?>

</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#reveal-password').change(function () {
            $('#new_password').attr('type',this.checked?'text':'password');
            $('#repeat_password').attr('type',this.checked?'text':'password');
            $('#old_password').attr('type',this.checked?'text':'password');
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>