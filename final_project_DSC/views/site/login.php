<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Sign In';

$fieldOptions1 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-envelope form-control-feedback'></span>"
];

$fieldOptions2 = [
    'options' => ['class' => 'form-group has-feedback'],
    'inputTemplate' => "{input}<span class='glyphicon glyphicon-lock form-control-feedback'></span>"
];
?>

<div class="login-box" >
    <center><h4><a href="#"><b class="text-success">DEL DATA SCIENCE CLUB</b></a></h4></center>
    <div class="login-logo">
        <?php
            $urlImage = Yii::getAlias('@web').'/images/logo.png';
        ?>    
        <img src="<?= $urlImage;?>" alt="Logo Image" style="height: 35%;width: 35%;"/>
        
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Silahkan LOGIN Ke Dalam Aplikasi</p>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => false]); ?>

        <?= $form
            ->field($model, 'username', $fieldOptions1)
            ->label(false)
            ->textInput(['placeholder' => $model->getAttributeLabel('username')]) ?>

        <?= $form
            ->field($model, 'password', $fieldOptions2)
            ->label(false)
            ->passwordInput(['placeholder' => $model->getAttributeLabel('password')]) ?>

        <div class="row">
            <div class="col-xs-8">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
            <!-- /.col -->
            <br>
            <div class="col-xs-12">
                <?= Html::submitButton('LOGIN', ['class' => 'btn btn-success btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <!-- /.col -->
        </div>


        <?php ActiveForm::end(); ?>
        <hr>
        <p class="text-danger">Belum Memiliki Akun?, Silahkan Klik "DAFTAR AKUN (REGISTER)"</p>
        <div class="social-auth-links text-center">
            

            <?= Html::a('<span class="fa fa-vcard"></span>DAFTAR AKUN (REGISTER)', ['site/register'], ['class' => 'btn btn-block btn-social btn-facebook btn-flat']) ?>
        </div>

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
