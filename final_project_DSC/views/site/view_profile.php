<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Data Profile';

?>

<?php
        
    Modal::begin([
        'header' => '<center><h4>FORM PROFILE</h4></center>',
        'id' => 'modal-data',
        'size' => 'modal-md',
        'options' => [
              'tabindex' => false, 
          ],
    ]);

    echo '<div id="content-modal-data"></div>';
    Modal::end();
?>

<?php
        
    Modal::begin([
        'header' => '<center><h4>FORM UPDATE PASSWORD</h4></center>',
        'id' => 'modal-data2',
        'size' => 'modal-md',
        'options' => [
              'tabindex' => false, 
          ],
    ]);

    echo '<div id="content-modal-data2"></div>';
    Modal::end();
?>

<div class="kategori-index">

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-6">
            <div class="box box-success">
                <div class="box-header with-border">
                    <center>
                      <h3 class="text-success">DATA PROFILE USER</h3>
                    </center>
                    <p align="right">
                        <?= Html::button('<span class="fa fa-user-circle-o"></span> UPDATE PROFILE', ['id' => 'modalButton', 'value' => \yii\helpers\Url::to(['site/updateuser','id'=>$id]), 'class' => 'btn btn-success']) ?>

                        <?= Html::button('<span class="glyphicon glyphicon-pencil"></span> UPDATE PASSWORD', ['id' => 'modalButton2', 'value' => \yii\helpers\Url::to(['updatepassword','id'=>$id]), 'class' => 'btn btn-primary']) ?>
                    </p>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= DetailView::widget([
                          'model' => $model,
                          'attributes' => [
                              'nim',
                              'nama_user',
                              'username',
                              'email',
                              'telp',
                              
                              [
                                'attribute' => 'status',
                                'format' => 'raw',
                                'value' => function ($data) {
                                  if ($data->status == 10)
                                    return "<span class='label label-success'>" . 'USER AKTIF' . "</span>";
                                  else
                                    return "<span class='label label-danger'>" . 'USER NON AKTIF' . "</span>";
                                }
                              ],

                              [
                                'attribute' => 'Role',
                                'format' => 'raw',
                                'value' => function ($data) {
                                  $roles = [];
                                  foreach ($data->roles as $role) {
                                    $roles[] = $role->item_name;
                                  }
                                  return "<span class='label label-danger'>" . (implode(', ', $roles)). "</span>";
                                }
                              ],

                              [
                                'attribute'=>'foto',
                                'format' => 'raw',
                                'value' => function ($data) {
                                  if (is_null($data->foto)){
                                    return "Foto User Belum Ada";
                                  }
                                    
                                  else{
                                    $urlImage = Yii::getAlias('@web').'/foto_profil/'.$data->foto;
                                    return '<img src="'.$urlImage.'" style="height: 100px;width: 100px;"/>';
                                  }
                                    
                                }

                                

                                
                             ],
                              
                                 
                          ],
                      ]) ?>

                </div>
            </div>
        </div>
    </div>

</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#modalButton').click(function () {
            $('#modal-data').modal('show')
                .find('#content-modal-data')
                .load($(this).attr('value'));
        });

        $('#modalButton2').click(function () {
            $('#modal-data2').modal('show')
                .find('#content-modal-data2')
                .load($(this).attr('value'));
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>