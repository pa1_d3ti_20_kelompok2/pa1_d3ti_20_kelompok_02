<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;
use skeeks\yii2\ckeditor\CKEditorWidget;
use skeeks\yii2\ckeditor\CKEditorPresets;

/* @var $this yii\web\View */
/* @var $model app\models\Materi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="materi-form">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <center><h3>FORM UPDATE ADRT</h3></center>
                    <p align="right">
                        <?= Html::a('<span class="glyphicon glyphicon-step-backward"></span>Kembali', ['adrt/view'], ['class' => 'btn btn-default']) ?>
                    </p>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php $form = ActiveForm::begin([]); ?>

                    <?= $form->field($model, 'file')->widget(FileInput::classname(), 
                        [
                            'pluginOptions'=>[
                                'allowedFileExtensions'=>['jpg','jpeg','png','pdf','docx','doc'],
                                'dropZoneEnabled'=> false,
                                'browseLabel' =>  'Upload File ADRT'
                            ],
                        ]) 
                    ?>

                   <?= $form->field($model, 'deskripsi_adrt')->widget(CKEditorWidget::className(), [
                        'preset' => CKEditorPresets::FULL
                    ]) ?>

                    

                    <div class="form-group">
                        <center><?= Html::submitButton('SIMPAN DATA ADRT', ['class' => 'btn btn-success']) ?></center>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    

</div>
