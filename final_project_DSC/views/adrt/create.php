<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Adrt */

$this->title = 'Tambah Program Kerja';

?>
<div class="adrt-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
