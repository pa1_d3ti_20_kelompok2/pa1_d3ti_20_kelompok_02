<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Adrt */

$this->title = 'Update ADRT';

?>
<div class="adrt-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
