<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Materi */

$this->title = 'View ADRT';
/*$this->params['breadcrumbs'][] = ['label' => 'Materis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
\yii\web\YiiAsset::register($this);
?>
<div class="materi-view">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <table class="table">
                        <tr>
                            <td colspan="2" align="center"><h4> <b class="text-success">ANGGARAN DASAR dan ANGGARAN RUMAH TANGGA</b></h4></td>
                            <td align="right">
                                <?php
                                    $url = Url::base().'/upload/adrt/'.$model->file_adrt;
                                    echo Html::a('<i class="fa fa-download"></i> Download File ADRT', $url, ['class' => 'btn btn-default','target' => '_blank']); 
                                ?>
                                <?php
                                    if($role->item_name == 'PEMBINA' || $role->item_name == 'ADMINISTRATOR' || $role->item_name == 'PENGURUS'){
                                        echo Html::a('<span class="glyphicon glyphicon-pencil"></span> UPDATE ADRT', ['update', 'id' => $model->id_adrt], ['class' => 'btn btn-success']);
                                    } 
                                     
                                ?>
                                
                            </td>
                        </tr>
                    </table>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?= $model->deskripsi_adrt;?>
                    
                </div>
                <div class="box-footer with-border">
                    <p align="right">
                        <i>Last Update</i> : <?=$model->updated_at;?>
                        <br>
                        <i>Update By</i> : <?=$model_user->nama_user;?>
                    </p>
                </div>
            </div>
        </div>
    </div>
    
</div>
