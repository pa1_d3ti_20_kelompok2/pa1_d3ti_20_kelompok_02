<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubmitTantangan */

$this->title = 'Create Submit Tantangan';
$this->params['breadcrumbs'][] = ['label' => 'Submit Tantangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="submit-tantangan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
