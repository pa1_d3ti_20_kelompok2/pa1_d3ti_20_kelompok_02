<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\SubmitTantangan */

$this->title = 'Update Submit Tantangan: ' . $model->id_tantangan;
$this->params['breadcrumbs'][] = ['label' => 'Submit Tantangans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_tantangan, 'url' => ['view', 'id_tantangan' => $model->id_tantangan, 'id_user' => $model->id_user]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="submit-tantangan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
