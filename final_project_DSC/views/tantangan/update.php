<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tantangan */

$this->title = 'Update Tantangan';

?>
<div class="tantangan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
