<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\select2\Select2;
use yii\db\Expression;
/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tantangan';

//$this->params['breadcrumbs'][] = $this->title;
?>

<?php
        
    Modal::begin([
        'header' => '<center><h4>DATA</h4></center>',
        'id' => 'modal-data',
        'size' => 'modal-md',
        'options' => [
              'tabindex' => false, 
          ],
    ]);

    echo '<div id="content-modal-data"></div>';
    Modal::end();
?>

<?php
    $bulan = array(
                '01' => 'Januari',
                '02' => 'Februari',
                '03' => 'Maret',
                '04' => 'April',
                '05' => 'Mei',
                '06' => 'Juni',
                '07' => 'Juli',
                '08' => 'Agustus',
                '09' => 'September',
                '10' => 'Oktober',
                '11' => 'November',
                '12' => 'Desember',
            );
?>

<div class="kategori-index">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="box box-success">
                <div class="box-header with-border">
                    <table class="table">
                        <tr>
                            <td colspan="2" align="center"><h4>TANTANGAN <b class="text-success">DEL DATA SCIENCE CLUB</b></h4></td>
                            
                            <td align="right">
                                <?php
                                    if($role->item_name == 'PEMBINA' || $role->item_name == 'ADMINISTRATOR' || $role->item_name == 'PENGURUS'){
                                        echo Html::a('<span class="glyphicon glyphicon-cloud-upload"></span> UPLOAD TANTANGAN', ['tantangan/create'], ['class' => 'btn btn-success']);
                                    } 
                                     
                                ?>
                            </td>
                        </tr>
                    </table>
                    <p align="right">
                        Waktu Sekarang :
                        <?php
                            date_default_timezone_set('Asia/Jakarta');
                            $time = date('d-m-Y H:i');
                            //echo "<b>".$time.'</b>';

                            echo date('d',strtotime($time)).' '.($bulan[date('m',strtotime($time))]).' '.date('Y',strtotime($time)).' | <b class="text-success">'.date('h',strtotime($time)).':'.date('i',strtotime($time)).'</b>';
                        ?>
                    </p>
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                        if(is_null($tantangan)){
                            echo "Tantangan Saat Ini Belum Ada";
                        }else{
                            
                            $i = 1;
                            foreach ($tantangan as $key => $value) {
                                                           
                    ?>
                            <ul class="products-list product-list-in-box">
                                <li class="item">
                                  <div class="product-img">
                                    <i class="fa fa-file-code-o fa-3x"></i>
                                  </div>
                                  <div class="product-info">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <?php
                                                    $now = strtotime($time); 
                                                    $open = strtotime($value['waktu_buka']);
                                                    if ($now >= $open) {
                                                        echo Html::a('<h4>'.$value['judul_tantangan'].'</h4>', ['tantangan/view','id'=>$value['id_tantangan']], ['class' => '']);
                                                    }
                                                    else{
                                                        echo '<h4>'.$value['judul_tantangan'].'</h4>';
                                                    }
                                                     
                                                ?>
                                            </td>
                                            <td align="right">
                                                <?php
                                                    if($role->item_name == 'PEMBINA' || $role->item_name == 'ADMINISTRATOR' || $role->item_name == 'PENGURUS'){
                                                        echo Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $value['id_tantangan']], ['class' => 'btn btn-warning btn-sm','title' => 'EDIT TANTANGAN',
                                                        'data-toggle' => 'tooltip']);
                                                        echo "&nbsp";
                                                        echo Html::a('<span class="glyphicon glyphicon-trash"></span>', ['delete', 'id' => $value['id_tantangan']], ['class' => 'btn btn-danger btn-sm','title' => 'HAPUS TANTANGAN',
                                                                'data-toggle' => 'tooltip','data' => [
                                                                'confirm' => 'Apakah anda yakin menghapus data ini?',
                                                                'method' => 'post',
                                                                
                                                            ],
                                                        ]);
                                                    } 
                                                     
                                                ?>
                                            </td>
                                        </tr>
                                    </table>
                                    

                                    <a href="javascript:void(0)" class="product-title">
                                      <span class="label label-info pull-right"><?= "Tantangan ".$i;?></span></a>
                                    <span class="product-description">
                                        <table class="table">
                                            <tr>
                                                <td>Open : 
                                                    <?php
                                                        echo date('d',strtotime($value['waktu_buka'])).' '.($bulan[date('m',strtotime($value['waktu_buka']))]).' '.date('Y',strtotime($value['waktu_buka'])).' | <b class="text-success">'.date('h',strtotime($value['waktu_buka'])).':'.date('i',strtotime($value['waktu_buka'])).'</b>';
                                                    ?>
                                                </td>
                                                <td>Close :
                                                    <?php
                                                        echo date('d',strtotime($value['waktu_tutup'])).' '.($bulan[date('m',strtotime($value['waktu_tutup']))]).' '.date('Y',strtotime($value['waktu_tutup'])).' | <b class="text-success">'.date('h',strtotime($value['waktu_tutup'])).':'.date('i',strtotime($value['waktu_tutup'])).'</b>';
                                                    ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </span>
                                  </div>
                                </li>
                               
                            </ul>
                    <?php
                                $i++;
                            }        
                        }
                    ?>

                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="box box-solid box-success">
                <div class="box-header with-border">
                    <center>
                      KETERANGAN TANTANGAN
                    </center>
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table">
                        <tr>
                            <th>Filter Tantangan</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                    echo Select2::widget([
                                        'name' => 'id_tahun',
                                        'data' => $arrayTahun,
                                        'options' => [
                                            'placeholder' => 'Filter Data Berdasarkan Tahun ...',
                                            'id'=>'id_tahun'
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                ?>
                            </td>
                        </tr>


                        <tr>
                            <th>Pembina</th>
                        </tr>
                    <?php
                        foreach ($pembina as $key => $value) {
                    ?>
                        <tr align="center">
                            <td><h3 class="label label-primary"><?= $value['nama_user'];?></h3></td>
                        </tr>
                    <?php        
                        }
                    ?>
                        
                    </table>
                    
                </div>
            </div>

            <div class="box box-solid box-danger">
                <div class="box-header with-border">
                    <center>
                      DAFTAR ANGGOTA
                    </center>
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table">
                        <tr>
                            <th>Jumlah Anggota : <?= Html::a($countAnggota.' orang', ['pengurus/list-user','role'=>'ANGGOTA'], ['class' => 'text-danger','title' => 'LIHAT DAFTAR ANGGOTA KESELURUHAN','data-toggle' => 'tooltip',]) ?></th>
                        </tr>
                        <tr>
                       
                    </table>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'nim',
                            'nama_user',
                            
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#modalButton').click(function () {
            $('#modal-data').modal('show')
                .find('#content-modal-data')
                .load($(this).attr('value'));
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>