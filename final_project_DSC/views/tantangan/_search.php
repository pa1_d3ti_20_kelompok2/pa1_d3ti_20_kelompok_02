<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TantanganSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tantangan-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_tantangan') ?>

    <?= $form->field($model, 'judul_tantangan') ?>

    <?= $form->field($model, 'deskripsi_tantangan') ?>

    <?= $form->field($model, 'waktu_buka') ?>

    <?= $form->field($model, 'waktu_tutup') ?>

    <?php // echo $form->field($model, 'file_tantangan') ?>

    <?php // echo $form->field($model, 'id_setting') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
