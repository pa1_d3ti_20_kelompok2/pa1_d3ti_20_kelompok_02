<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SubmitTantangan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="submit-tantangan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nilai')->textInput(['placeholder' => 'INPUT NILAI'])->label(false) ?>

    <div class="form-group">
        <center><?= Html::submitButton('SIMPAN NILAI', ['class' => 'btn btn-success']) ?></center>
    </div>

    <?php ActiveForm::end(); ?>

</div>
