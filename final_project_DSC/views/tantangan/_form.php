<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use skeeks\yii2\ckeditor\CKEditorWidget;
use skeeks\yii2\ckeditor\CKEditorPresets;
use kartik\widgets\DateTimePicker;
/* @var $this yii\web\View */
/* @var $model app\models\Tantangan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tantangan-form">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <center><h3>FORM UPLOAD TANTANGAN</span></h3></center>
                    <p align="right">
                        <?= Html::a('<span class="glyphicon glyphicon-step-backward"></span>Kembali', ['tantangan/index'], ['class' => 'btn btn-default']) ?>
                    </p>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'judul_tantangan')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'deskripsi_tantangan')->widget(CKEditorWidget::className(), [
                        'options' => ['rows' => 3],
                        'preset' => CKEditorPresets::FULL
                    ]) ?>

                    <?= $form->field($model, 'file')->widget(FileInput::classname(), 
                        [
                            'pluginOptions'=>[
                                'allowedFileExtensions'=>['jpg','jpeg','png','pdf','docx','doc','zip','rar'],
                                'dropZoneEnabled'=> false,
                                'browseLabel' =>  'Upload Deskripsi File Tantangan'
                            ],
                        ]) 
                    ?>
                    <table class="table">
                        <tr>
                            <td>
                                <?= $form->field($model, 'waktu_buka')->widget(DateTimePicker::classname(), [
                                    'options' => ['placeholder' => 'Waktu Buka'],
                                    'pluginOptions' => [
                                        'autoclose' => true
                                    ]
                                ]); ?>
                            </td>
                            <td>
                                <?= $form->field($model, 'waktu_tutup')->widget(DateTimePicker::classname(), [
                                    'options' => ['placeholder' => 'Waktu Tutup'],
                                    'pluginOptions' => [
                                        'autoclose' => true
                                    ]
                                ]); ?>
                            </td>
                        </tr>
                    </table>

                    <div class="form-group">
                        <center><?= Html::submitButton('SIMPAN TANTANGAN', ['class' => 'btn btn-success']) ?></center>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
