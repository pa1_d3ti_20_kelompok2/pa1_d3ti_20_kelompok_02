<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tantangan */

$this->title = 'Upload Tantangan';

?>
<div class="tantangan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
