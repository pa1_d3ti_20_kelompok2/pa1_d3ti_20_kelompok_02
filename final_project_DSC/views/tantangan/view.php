<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use kartik\file\FileInput;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Tantangan */

$this->title = 'View Tantangan';

?>
<div class="tantangan-view">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <?php
                if($role->item_name == 'PEMBINA' || $role->item_name == 'ADMINISTRATOR' || $role->item_name == 'PENGURUS'){
            ?>
                <div class="box box-widget">
                    <div class="box-header with-border">
                        <table class="table">
                            <tr align="right">
                                <td>
                                    <?php
                                         echo Html::a('<span class="fa fa-drivers-license-o"></span> LIHAT DATA STATUS SUBMIT TANTANGAN', ['status-submit', 'id_tantangan' => $model->id_tantangan], ['class' => 'btn btn-primary']);
                                         echo "&nbsp";
                                         echo Html::a('<span class="fa fa-tasks"></span> REKAPITULASI NILAI TANTANGAN', ['rekapitulasi-nilai', 'id_tantangan' => $model->id_tantangan], ['class' => 'btn btn-default']);
                                         echo "&nbsp";
                                         echo Html::a('<span class="glyphicon glyphicon-pencil"></span> UPDATE TANTANGAN', ['update', 'id' => $model->id_tantangan], ['class' => 'btn btn-warning']);
                                         echo "&nbsp";
                                         echo Html::a('<span class="glyphicon glyphicon-trash"></span> HAPUS', ['delete', 'id' => $model->id_tantangan], ['class' => 'btn btn-danger','data' => [
                                                'confirm' => 'Apakah anda yakin menghapus data ini?',
                                                'method' => 'post',
                                            ],
                                        ]);
                                    ?>
                                    
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            <?php
                }
            ?>
            <ul class="timeline">
                <!-- timeline time label -->
                <li class="time-label">
                    <span class="bg-green">
                        <?php
                            $bulan = array(
                                        '01' => 'Januari',
                                        '02' => 'Februari',
                                        '03' => 'Maret',
                                        '04' => 'April',
                                        '05' => 'Mei',
                                        '06' => 'Juni',
                                        '07' => 'Juli',
                                        '08' => 'Agustus',
                                        '09' => 'September',
                                        '10' => 'Oktober',
                                        '11' => 'November',
                                        '12' => 'Desember',
                                    );

                            /*echo date('d',strtotime($model->waktu_buka)).' '.($bulan[date('m',strtotime($model->waktu_buka))]).' '.date('Y',strtotime($model->waktu_buka));

                            echo " - ".date('d',strtotime($model->waktu_tutup)).' '.($bulan[date('m',strtotime($model->waktu_tutup))]).' '.date('Y',strtotime($model->waktu_tutup));*/
                        ?>
                        FORM UPLOAD TANTANGAN
                    </span>
                    <span class="time"><?= Html::a('<span class="glyphicon glyphicon-step-backward"></span>Kembali', ['tantangan/index'], ['class' => 'btn btn-default']) ?></span>
                </li>
                <!-- /.timeline-label -->
                <!-- timeline item -->
                <li>
                    <!-- timeline icon -->
                    <i class="fa fa-file-code-o bg-red"></i>
                    <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> Waktu Sekarang :
                            <?php
                                date_default_timezone_set('Asia/Jakarta');
                                $time = date('d-m-Y H:i');

                                echo date('d',strtotime($time)).' '.($bulan[date('m',strtotime($time))]).' '.date('Y',strtotime($time)).' | <b class="text-success">'.date('h',strtotime($time)).':'.date('i',strtotime($time)).'</b>';
                            ?>
                        </span>

                        <h3 class="timeline-header">
                            <b class="text-primary"><?= $model->judul_tantangan;?></b>
                        </h3>

                        <div class="timeline-body">
                            <?=$model->deskripsi_tantangan?>
                        </div>

                        
                    </div>
                </li>
                <!-- END timeline item -->
                <li>
                    <!-- timeline icon -->
                    <i class="fa fa-cloud-download bg-yellow"></i>
                    <div class="timeline-item">
                       
                        <div class="timeline-body">
                            <?php
                                if ($model->file_tantangan!=NULL) {
                                    $url = Url::base().'/upload/tantangan/'.$model->file_tantangan;
                                    echo Html::a('<i class="fa fa-download"></i> Download File Tantangan', $url, ['class' => 'btn btn-default','target' => '_blank']); 
                                     
                                }else{
                                    echo '<label class="label label-danger">File Tantangan Kosong</label>';
                                }
                                
                            ?>
                        </div>

                        <div class="timeline-footer">
                            <table class="table">
                                <tr>
                                    <td>Dibuka Pada : 
                                        <?php
                                            echo date('d',strtotime($model->waktu_buka)).' '.($bulan[date('m',strtotime($model->waktu_buka))]).' '.date('Y',strtotime($model->waktu_buka)).' | <b class="text-success">'.date('h',strtotime($model->waktu_buka)).':'.date('i',strtotime($model->waktu_buka)).'</b>';
                                        ?>
                                    </td>
                                    <td>Submit Tantangan Berakhir Pada:
                                        <?php
                                            echo '<h4>'.date('d',strtotime($model->waktu_tutup)).' '.($bulan[date('m',strtotime($model->waktu_tutup))]).' '.date('Y',strtotime($model->waktu_tutup)).' | <b class="text-success">'.date('h',strtotime($model->waktu_tutup)).':'.date('i',strtotime($model->waktu_tutup)).'</b>'.'</h4>';
                                        ?>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </li>

                <li>
                  <i class="fa fa-cloud-upload bg-blue"></i>

                  <div class="timeline-item">
                    <h3 class="timeline-header"><b class="text-primary">Submit Tantangan Anda</b></h3>

                    <div class="timeline-body">
                        <?php
                            date_default_timezone_set('Asia/Jakarta');
                            $time = date('d-m-Y H:i');
                            $now = strtotime($time); 
                            $close = strtotime($model->waktu_tutup);

                            if ($now <= $close) {
                        ?>

                                <?php 
                                    if ($model_submit->isNewRecord){
                                        $action = 'submit-tantangan/create';
                                    }else{
                                        $action = 'submit-tantangan/update';
                                    }
                                    $form = ActiveForm::begin([
                                        'action' => [$action,'id_tantangan'=>$model->id_tantangan,'id_user'=>$user->id],
                                        'method' => 'post',
                                    ]); 
                                ?>

                                <?= $form->field($model_submit, 'file')->widget(FileInput::classname(), 
                                    [
                                        'pluginOptions'=>[
                                            'allowedFileExtensions'=>['jpg','jpeg','png','pdf','docx','doc','zip','rar'],
                                            'dropZoneEnabled'=> true,
                                            'browseLabel' =>  'Submit File Tantangan Anda'
                                        ],
                                    ])->label(false); 
                                ?>

                                <div class="form-group">
                                    <center><?= Html::submitButton('SUBMIT FILE TANTANGAN', ['class' => 'btn btn-primary']) ?></center>
                                </div>

                                <?php ActiveForm::end(); ?>
                        <?php
                            }
                        ?>
                        

                    </div>
                    <div class="timeline-footer">
                      <table class="table">
                          <tr>
                              <td>Status Submit</td>
                              <td>
                                  <?php
                                    if (is_null($submit_tantangan)) {
                                        echo "-";
                                    }else{
                                        echo '<label class="label label-success">Telah Melakukan Submit Tantangan</label>';
                                    }
                                  ?>
                              </td>
                          </tr>
                          <tr>
                              <td>File Yang Disubmit</td>
                              <td>
                                  <?php
                                    if (is_null($submit_tantangan)) {
                                        echo "-";
                                    }else{
                                        $url = Url::base().'/upload/submit_tantangan/'.$submit_tantangan->file_submit;
                                        echo Html::a('<i class="fa fa-download"></i> File Yang Telah Disubmit', $url, ['class' => 'btn btn-default','target' => '_blank']);
                                    }
                                  ?>
                              </td>
                          </tr>
                      </table>
                    </div>
                  </div>
                </li>
                        
            </ul>
        </div>
    </div>
</div>
