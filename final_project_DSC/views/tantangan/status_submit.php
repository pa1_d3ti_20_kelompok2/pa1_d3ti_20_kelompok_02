<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\select2\Select2;
use app\models\SubmitTantangan;


/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'STATUS SUBMIT';

?>

<?php
        
    Modal::begin([
        'header' => '<center><h4>FORM NILAI</h4></center>',
        'id' => 'modal-data',
        'size' => 'modal-sm',
        'options' => [
              'tabindex' => false, 
          ],
    ]);

    echo '<div id="content-modal-data"></div>';
    Modal::end();
?>

<div class="kategori-index">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-solid box-success">
                <div class="box-header with-border">
                    <center>
                      Daftar Status Submit User Pada "<b><?= $tantangan->judul_tantangan;?></b>" 
                    </center>
                    
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <p align="right">
                        <?= Html::a('<span class="fa fa-tasks"></span> REKAPITULASI NILAI TANTANGAN', ['rekapitulasi-nilai', 'id_tantangan' => $tantangan->id_tantangan], ['class' => 'btn btn-default']);?>
                    </p>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'nim',
                            'nama_user',

                            [
                                'header' => 'STATUS SUBMIT',
                                'attribute' => '',
                                'format' =>'raw',
                                'value' => function ($model) use ($tantangan){
                                  if ($model->id!=NULL) {
                                    $submit_tantangan = SubmitTantangan::find()->where(['id_tantangan'=>$tantangan->id_tantangan])->andWhere(['id_user'=>$model->id])->one();
                                    if (is_null($submit_tantangan)) {
                                        return '<h4 class="label label-danger">Belum Submit</h4>';
                                    }else{
                                        return '<h4 class="label label-success">Sudah Submit</h4>';
                                    }
                                    
                                  }else{
                                      return "-";
                                  }
                                },
                            ],

                            [
                                'header' => 'FILE SUBMIT',
                                'attribute' => '',
                                'format' =>'raw',
                                'value' => function ($model) use ($tantangan){
                                  if ($model->id!=NULL) {
                                    $submit_tantangan = SubmitTantangan::find()->where(['id_tantangan'=>$tantangan->id_tantangan])->andWhere(['id_user'=>$model->id])->one();
                                    if (is_null($submit_tantangan)) {
                                        return 'File Submit Kosong';
                                    }else{
                                        $url = Url::base().'/upload/submit_tantangan/'.$submit_tantangan->file_submit;
                                        return Html::a('<i class="fa fa-download"></i> Download File Tantangan', $url, ['class' => 'btn btn-default','target' => '_blank']); 
                                        
                                    }
                                    
                                  }else{
                                      return "-";
                                  }
                                },
                            ],

                            [
                                'header' => 'NILAI',
                                'attribute' => '',
                                'format' =>'raw',
                                'value' => function ($model) use ($tantangan){
                                  if ($model->id!=NULL) {
                                    $submit_tantangan = SubmitTantangan::find()->where(['id_tantangan'=>$tantangan->id_tantangan])->andWhere(['id_user'=>$model->id])->one();
                                    if (is_null($submit_tantangan)) {
                                        return Yii::$app->formatter->asDecimal(0,2);
                                    }else{
                                        if (is_null($submit_tantangan->nilai)) {
                                            return Yii::$app->formatter->asDecimal(0,2);
                                        }else{
                                            return Yii::$app->formatter->asDecimal($submit_tantangan->nilai,2);
                                        }
                                        
                                    }
                                    
                                  }else{
                                      return "-";
                                  }
                                },
                            ],

                            [
                                'options' => [
                                    'width' => '150px',
                                ],
                                'header' => 'INPUT NILAI',

                                'class' => 'yii\grid\ActionColumn',
                                    'template' => '{updatenilai}',
                                    'buttons' => [
                                        'updatenilai' => function($url, $model) use ($tantangan) {
                                            $submit_tantangan = SubmitTantangan::find()->where(['id_tantangan'=>$tantangan->id_tantangan])->andWhere(['id_user'=>$model->id])->one();
                                            if (!is_null($submit_tantangan)) {
                                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                                            'class' => 'btn btn-warning',
                                                            'title' => 'ENTER NILAI',
                                                            'id' => 'modalButton',
                                                            'onclick' => "
                                              $.ajax({
                                                url: '".Url::to(['tantangan/update-nilai-tantangan','id_tantangan' => $tantangan->id_tantangan,'id_user'=>$model->id])."',
                                                success:function(data){
                                                  $('#title').html('Detail Organisasi');
                                                  $('#content-modal-data').html(data);
                                                  $('#modal-data').modal('show');
                                                },
                                                error:function(xhr, ajaxOptions, throwError){
                                                  alert(xhr.responseText);
                                                }
                                              })
                                              ",]);
                                            }else{
                                                return "-";
                                            }
                                        },


                                        
                                    ]
                            ],

                            
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#modalButton').click(function () {
            $('#modal-data').modal('show')
                .find('#content-modal-data')
                .load($(this).attr('value'));
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>