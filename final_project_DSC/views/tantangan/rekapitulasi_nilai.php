<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\select2\Select2;
use app\models\SubmitTantangan;
use kartik\grid\GridView;
use kartik\mpdf\Pdf;
/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'REKAPITULASI NILAI';

?>

<?php
        
    Modal::begin([
        'header' => '<center><h4>FORM NILAI</h4></center>',
        'id' => 'modal-data',
        'size' => 'modal-sm',
        'options' => [
              'tabindex' => false, 
          ],
    ]);

    echo '<div id="content-modal-data"></div>';
    Modal::end();
?>

<div class="kategori-index">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            
            <?= GridView::widget([
                  'dataProvider'=>$dataProvider,
                  //'filterModel'=>$searchModel,
                  /*'summary'=>'',*/
                  'showPageSummary'=>true,
                  'pjax'=>true,
                  'striped'=>true,
                  'hover'=>true,
                  'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                  'responsive'=>true,
                  'containerOptions' => ['style' => 'overflow: auto'],
                  'export'=>['fontAwesome'=>true,
                            'showConfirmAlert'=>false,
                            'label'=>'Download',
                            'header'=>'',
                            'options'=>[
                              'class' => 'btn btn-info',
                            ],

                          ],
                  'exportConfig'=>[
                      
                      GridView::PDF =>[
                                'icon' => 'file-pdf-o',
                                'iconOptions' => ['class' => 'text-success'],
                                'filename' => 'Rekapitulasi_Nilai_',
                                'alertMsg' =>null,
                                'options' => ['title' => 'Portable Document Format'],
                                'mime' => 'application/pdf',
                                'format' => Pdf::FORMAT_A4,
                                'config' => [
                                    'methods' => [
                                        'SetHeader' => ['Dicetak dari: Aplikasi Del Data Science Club ||Dicetak tanggal: ' . date("d-M-Y")],
                                        /*'SetHeader' =>'',*/
                                        'SetFooter' => ['||Halaman {PAGENO}'],
                                    ],                      
                                    'contentBefore'=>'Rekapitulasi Nilai Tantangan<br>',
                                    'contentAfter'=>'',
                                ]
                              ],
                      GridView::EXCEL =>[
                                'icon' => 'file-excel-o',
                                'iconOptions' => ['class' => 'text-success'],
                                'filename' => 'Rekapitulasi_Nilai_',
                                
                              ],
                  ],
                  'toolbar'=>['{toggleData}{export}'],
                  'panel'=>['type'=>'primary', 'heading'=>'REKAPITULASI NILAI'],
                  

                  'columns'=>[
                        ['class' => 'yii\grid\SerialColumn'],
                        'nim',
          
                        [
                            'attribute' => 'nama_user',
                            'value' => 'nama_user',
                            'pageSummary' => 'RATA-RATA NILAI',

                        ],

                        [
                            'header' => 'NILAI',
                            'attribute' => '',
                            'format' =>'raw',
                            'value' => function ($model) use ($tantangan){
                              if ($model->id!=NULL) {
                                $submit_tantangan = SubmitTantangan::find()->where(['id_tantangan'=>$tantangan->id_tantangan])->andWhere(['id_user'=>$model->id])->one();
                                if (is_null($submit_tantangan)) {
                                    return Yii::$app->formatter->asDecimal(0,2);
                                }else{
                                    if (is_null($submit_tantangan->nilai)) {
                                        return Yii::$app->formatter->asDecimal(0,2);
                                    }else{
                                        return Yii::$app->formatter->asDecimal($submit_tantangan->nilai,2);
                                    }
                                    
                                }
                                
                              }else{
                                  return "-";
                              }
                            },
                            'pageSummary' => true,
                            'pageSummaryFunc' => GridView::F_AVG,
                        ],
                    
                  ],
                ]);
            ?>
                    
                
        </div>
    </div>

</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#modalButton').click(function () {
            $('#modal-data').modal('show')
                .find('#content-modal-data')
                .load($(this).attr('value'));
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>