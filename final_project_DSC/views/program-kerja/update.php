<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProgramKerja */

$this->title = 'Update Program Kerja';

?>
<div class="program-kerja-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
