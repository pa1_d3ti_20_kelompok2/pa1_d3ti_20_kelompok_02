<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProgramKerjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Program Kerja';
?>
<div class="program-kerja-index">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <center><h3>Program Kerja</h3></center>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                        /*$events = array();
                        //Testing
                        $Event = new \yii2fullcalendar\models\Event();
                        $Event->id = 1;
                        $Event->title = 'Ensemble models and techniques';
                        $Event->start = "2020-06-01";
                        $Event->end = "2020-06-05";
                        $Event->nonstandard = [
                            'field1' => 'Something I want to be included in object #1',
                            'field2' => 'Something I want to be included in object #2',
                        ];
                        $events[] = $Event;

                        $Event = new \yii2fullcalendar\models\Event();
                        $Event->id = 2;
                        $Event->title = 'Testing 2';
                        $Event->start = date('Y-m-d\TH:i:s\Z',strtotime('tomorrow 6am'));
                        $events[] = $Event;*/

                    ?>

                    <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                              'events'=> $events,
                          ));
                    ?>
                </div>
            </div>
        </div>
    </div>
    

    

    <p>
        <?= Html::a('Create Program Kerja', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_program_kerja',
            'nama_program_kerja',
            'deskripsi_program_kerja',
            'tanggal_awal',
            'tanggal_akhir',
            //'id_setting',
            //'created_at',
            //'created_by',
            //'updated_at',
            //'updated_by',
            //'active',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
