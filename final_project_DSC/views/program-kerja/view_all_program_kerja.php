<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProgramKerjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Program Kerja';
?>


<div class="program-kerja-index">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <center><h3>DATA PROGRAM KERJA</h3></center>
                    <p align="right">
                        <?= Html::a('<span class="glyphicon glyphicon-step-backward"></span>Kembali', ['program-kerja/index'], ['class' => 'btn btn-default']) ?>

                        <?php 
                            if($role->item_name == 'PEMBINA' || $role->item_name == 'ADMINISTRATOR' || $role->item_name == 'PENGURUS'){
                                echo Html::a('<span class="glyphicon glyphicon-plus"></span> TAMBAH PROGRAM KERJA', ['create'], ['class' => 'btn btn-success']);
                            }
                            
                        ?>
                    </p>
                </div>
                
                <div class="box-body">
                    <?=GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'nama_program_kerja',

                            [
                                'attribute' => 'tanggal_awal',
                                'format' =>'raw',
                                'value' => function ($model){
                                    if ($model->tanggal_awal!=NULL) {
                                        $bulan = array(
                                            '01' => 'Januari',
                                            '02' => 'Februari',
                                            '03' => 'Maret',
                                            '04' => 'April',
                                            '05' => 'Mei',
                                            '06' => 'Juni',
                                            '07' => 'Juli',
                                            '08' => 'Agustus',
                                            '09' => 'September',
                                            '10' => 'Oktober',
                                            '11' => 'November',
                                            '12' => 'Desember',
                                        );

                                        return date('d',strtotime($model->tanggal_awal)).' '.($bulan[date('m',strtotime($model->tanggal_awal))]).' '.date('Y',strtotime($model->tanggal_awal));
                                    }else{
                                        return "-";
                                    }
                                }
                                
                            ],

                            [
                                'attribute' => 'tanggal_akhir',
                                'format' =>'raw',
                                'value' => function ($model){
                                    if ($model->tanggal_akhir!=NULL) {
                                        $bulan = array(
                                            '01' => 'Januari',
                                            '02' => 'Februari',
                                            '03' => 'Maret',
                                            '04' => 'April',
                                            '05' => 'Mei',
                                            '06' => 'Juni',
                                            '07' => 'Juli',
                                            '08' => 'Agustus',
                                            '09' => 'September',
                                            '10' => 'Oktober',
                                            '11' => 'November',
                                            '12' => 'Desember',
                                        );
                                        return date('d',strtotime($model->tanggal_akhir)).' '.($bulan[date('m',strtotime($model->tanggal_akhir))]).' '.date('Y',strtotime($model->tanggal_akhir));
                                    }else{
                                        return "-";
                                    }
                                }
                                
                            ],

                            [
                                'attribute' => 'file_program_kerja',
                                'format' =>'raw',
                                'value' => function ($model){
                                    if ($model->file_program_kerja!=NULL) {
                                        $url = Url::base().'/upload/program_kerja/'.$model->file_program_kerja;
                                        return Html::a('<i class="fa fa-download"></i> Download File', $url, ['class' => 'btn btn-default','target' => '_blank']); 
                                         
                                    }else{
                                        return "-";
                                    }
                                }
                                
                            ],

                            [
                                'options' => [
                                    'width' => '150px',
                                ],
                                'header' => 'AKSI',

                                'class' => 'yii\grid\ActionColumn',
                                    'template' => '{view} {update} {delete}',
                                    'buttons' => [
                                        'view' => function($url, $model) {
                                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',$url,['class' => 'btn btn-info btn-sm',
                                                        'title' => 'LIHAT DATA PROGRAM KERJA',]);
                                        },

                                        'update' => function($url, $model) use ($role) {
                                            if($role->item_name == 'PEMBINA' || $role->item_name == 'ADMINISTRATOR' || $role->item_name == 'PENGURUS'){
                                                return Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,['class' => 'btn btn-warning btn-sm','title' => 'EDIT DATA PROGRAM KERJA',]);
                                            }
                                        },

                                        'delete' => function ($url, $model) use ($role) {
                                            if($role->item_name == 'PEMBINA' || $role->item_name == 'ADMINISTRATOR' || $role->item_name == 'PENGURUS'){
                                                return  Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                                        'class' => 'btn btn-danger',
                                                        'title' => 'HAPUS DATA',
                                                        'data-toggle' => 'tooltip',
                                                        'data-method' => 'post',
                                                        'data-confirm' => 'Apakah anda yakin ingin menghapus data ini?'
                                                    ]);
                                            }
                                            
                                        },
                                    ]
                            ],

                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
</div>
