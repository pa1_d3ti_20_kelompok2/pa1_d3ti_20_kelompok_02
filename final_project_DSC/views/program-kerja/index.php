<?php

use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProgramKerjaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Program Kerja';
?>
<div class="program-kerja-index">
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-7">
            <div class="box box-widget">
                <div class="box-header with-border">
                    <center><h3>Kalender Program Kerja</h3></center>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                    <?= \yii2fullcalendar\yii2fullcalendar::widget(array(
                              'events'=> $events,
                          ));
                    ?>
                </div>
            </div>
        </div>

        <div class="col-lg-5 col-md-5 col-sm-5">
            <?php
                /*echo Select2::widget([
                    'name' => 'id_tahun',
                    'data' => $arrayTahun,
                    'options' => [
                        'placeholder' => 'Filter Data Berdasarkan Tahun ...',
                        'id'=>'id_tahun'
                    ],
                    'pluginOptions' => [
                        'allowClear' => true
                    ],
                ]);*/
            ?>

            <div class="box box-solid box-success">
                <div class="box-header with-border">
                    <center>PROGRAM KERJA</center>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                        echo Html::a('<span class="fa fa-eye"></span> LIHAT SEMUA PROGRA KERJA', ['program-kerja/view-all-program-kerja'], ['class' => 'btn btn-default btn-block btn-flat']);
                    ?>
                    <br>
                    <?php
                        $i=0;
                        foreach ($program_kerja_top10 as $key => $value) {
                            if ($i == 0) {
                                    $callout = "callout callout-info";
                                }else if($i == 1){
                                    $callout = "callout callout-warning";
                                }else if($i == 2){
                                    $callout = "callout callout-danger";
                                }
                                else if($i == 3){
                                    $callout = "callout callout-success";
                                }
                                else{
                                    $callout = "callout callout-warning";
                                }
                    ?>
                            <div class="<?= $callout;?>">
                                <h4>
                                    <?php
                                        echo Html::a('<span class="fa fa-calendar"></span> '.$value['nama_program_kerja'], ['program-kerja/view','id'=>$value['id_program_kerja']], ['class' => '']);
                                    ?>
                                </h4>

                                <p>
                                    <?php
                                        $bulan = array(
                                                  '01' => 'Januari',
                                                  '02' => 'Februari',
                                                  '03' => 'Maret',
                                                  '04' => 'April',
                                                  '05' => 'Mei',
                                                  '06' => 'Juni',
                                                  '07' => 'Juli',
                                                  '08' => 'Agustus',
                                                  '09' => 'September',
                                                  '10' => 'Oktober',
                                                  '11' => 'November',
                                                  '12' => 'Desember',
                                                );

                                        echo "( ".date('d',strtotime($value['tanggal_awal'])).' '.($bulan[date('m',strtotime($value['tanggal_awal']))]).' '.date('Y',strtotime($value['tanggal_awal']));
                                        echo " - ".date('d',strtotime($value['tanggal_akhir'])).' '.($bulan[date('m',strtotime($value['tanggal_akhir']))]).' '.date('Y',strtotime($value['tanggal_akhir']))." )";
                                    ?>
                                </p>
                            </div>
                    <?php
                            $i++;
                        }
                    ?>

                    
                    
                </div>
            </div>
        </div>
    </div>

</div>
