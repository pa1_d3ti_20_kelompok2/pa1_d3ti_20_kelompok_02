<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use skeeks\yii2\ckeditor\CKEditorWidget;
use skeeks\yii2\ckeditor\CKEditorPresets;
use kartik\date\DatePicker;
/* @var $this yii\web\View */
/* @var $model app\models\ProgramKerja */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="program-kerja-form">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <center><h3>FORM PROGRAM KERJA</h3></center>
                    <p align="right">
                        <?= Html::a('<span class="glyphicon glyphicon-step-backward"></span>Kembali', ['program-kerja/view-all-program-kerja'], ['class' => 'btn btn-default']) ?>
                    </p>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php $form = ActiveForm::begin(); ?>

                    <?= $form->field($model, 'nama_program_kerja')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'deskripsi_program_kerja')->widget(CKEditorWidget::className(), [
                        'options' => ['rows' => 4],
                        'preset' => CKEditorPresets::FULL
                    ]) ?>

                    <table class="table">
                        <tr>
                            <td>
                                <?= $form->field($model, 'tanggal_awal')->widget(DatePicker::classname(),
                                    [
                                        'value' => date('d-M-Y', strtotime('+2 days')),
                                        'options' => ['placeholder' => 'Masukkan Tanggal Awal ...'],
                                        'pluginOptions' => [
                                            'format' => 'yyyy-m-d',
                                            'todayHighlight' => true
                                        ]
                                    ])
                                ?>
                            </td>
                            <td>
                                <?= $form->field($model, 'tanggal_akhir')->widget(DatePicker::classname(),
                                    [
                                        'value' => date('d-M-Y', strtotime('+2 days')),
                                        'options' => ['placeholder' => 'Masukkan Tanggal Akhir ...'],
                                        'pluginOptions' => [
                                            'format' => 'yyyy-m-d',
                                            'todayHighlight' => true
                                        ]
                                    ])
                                ?>
                            </td>
                        </tr>
                    </table>
                    
                    <?= $form->field($model, 'file')->widget(FileInput::classname(), 
                        [
                            'pluginOptions'=>[
                                'allowedFileExtensions'=>['jpg','jpeg','png','pdf','docx','doc'],
                                'dropZoneEnabled'=> false,
                                'browseLabel' =>  'Upload File Program Kerja'
                            ],
                        ]) 
                    ?>


                    <div class="form-group">
                        <center><?= Html::submitButton('SIMPAN DATA PROGRAM KERJA', ['class' => 'btn btn-success']) ?></center>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
