<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProgramKerja */

$this->title = 'Tambah Program Kerja';

?>
<div class="program-kerja-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
