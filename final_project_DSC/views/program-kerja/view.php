<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\ProgramKerja */

$this->title = 'View Data Program Kerja';

?>
<div class="program-kerja-view">

    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="box box-widget">
            <div class="box-header with-border">
                <table class="table">
                    <tr>
                        
                        <td align="right">
                            <?= Html::a('<span class="glyphicon glyphicon-step-backward"></span>Kembali', ['program-kerja/view-all-program-kerja'], ['class' => 'btn btn-default']) ?>
                            <?php
                                if($role->item_name == 'PEMBINA' || $role->item_name == 'ADMINISTRATOR' || $role->item_name == 'PENGURUS'){
                                    echo Html::a('<span class="glyphicon glyphicon-pencil"></span> UPDATE MATERI', ['update', 'id' => $model->id_program_kerja], ['class' => 'btn btn-success']);
                                    echo "&nbsp";
                                    echo Html::a('<span class="glyphicon glyphicon-trash"></span> HAPUS', ['delete', 'id' => $model->id_program_kerja], ['class' => 'btn btn-danger','data' => [
                                            'confirm' => 'Apakah anda yakin menghapus data ini?',
                                            'method' => 'post',
                                        ],
                                    ]);
                                } 
                                 
                            ?>
                            
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <ul class="timeline">
                    <!-- timeline time label -->
                    <li class="time-label">
                        <span class="bg-green">
                            <?php
                                $bulan = array(
                                            '01' => 'Januari',
                                            '02' => 'Februari',
                                            '03' => 'Maret',
                                            '04' => 'April',
                                            '05' => 'Mei',
                                            '06' => 'Juni',
                                            '07' => 'Juli',
                                            '08' => 'Agustus',
                                            '09' => 'September',
                                            '10' => 'Oktober',
                                            '11' => 'November',
                                            '12' => 'Desember',
                                        );

                                echo date('d',strtotime($model->tanggal_awal)).' '.($bulan[date('m',strtotime($model->tanggal_awal))]).' '.date('Y',strtotime($model->tanggal_awal));

                                echo " - ".date('d',strtotime($model->tanggal_akhir)).' '.($bulan[date('m',strtotime($model->tanggal_akhir))]).' '.date('Y',strtotime($model->tanggal_akhir));
                            ?>
                        </span>
                    </li>
                    <!-- /.timeline-label -->
                    <!-- timeline item -->
                    <li>
                        <!-- timeline icon -->
                        <i class="fa fa-envelope bg-blue"></i>
                        <div class="timeline-item">
                            <span class="time"><i class="fa fa-calendar"></i></span>

                            <h3 class="timeline-header">
                                <b class="text-primary"><?= $model->nama_program_kerja;?></b>
                            </h3>

                            <div class="timeline-body">
                                <?=$model->deskripsi_program_kerja?>
                            </div>

                            
                        </div>
                    </li>
                    <!-- END timeline item -->
                    <li>
                        <!-- timeline icon -->
                        <i class="fa fa-cloud-download bg-yellow"></i>
                        <div class="timeline-item">
                           
                            <div class="timeline-body">
                                <?php
                                    if ($model->file_program_kerja!=NULL) {
                                        $url = Url::base().'/upload/program_kerja/'.$model->file_program_kerja;
                                        echo Html::a('<i class="fa fa-download"></i> Download File Program Kerja', $url, ['class' => 'btn btn-default','target' => '_blank']); 
                                         
                                    }else{
                                        echo '<label class="label label-danger">File Program Kerja Kosong</label>';
                                    }
                                    
                                ?>
                            </div>

                            <div class="timeline-footer">
                                <p align="right">
                                    <i>Last Update</i> : <?=$model->updated_at;?>
                                    <br>
                                    <i>Update By</i> : <?=$model_user->nama_user;?>
                                </p>
                            </div>
                        </div>
                    </li>
                    

                </ul>


            </div>
        </div>
    </div>

</div>
