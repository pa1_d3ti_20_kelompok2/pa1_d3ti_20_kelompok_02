<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $model app\models\Materi */

$this->title = 'View Materi';
/*$this->params['breadcrumbs'][] = ['label' => 'Materis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;*/
\yii\web\YiiAsset::register($this);
?>
<div class="materi-view">

    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="box box-widget">
            <div class="box-header with-border">
                <table class="table">
                    <tr>
                        <td colspan="2" align="center"><h4> <b class="text-success"><?= $model->judul_materi;?></b></h4></td>
                        <td align="right">
                            <?= Html::a('<span class="glyphicon glyphicon-step-backward"></span>Kembali', ['materi/view-materi','id_kategori'=> $model->id_kategori], ['class' => 'btn btn-default']) ?>
                            <?php
                                if($role->item_name == 'PEMBINA' || $role->item_name == 'ADMINISTRATOR' || $role->item_name == 'PENGURUS'){
                                    echo Html::a('<span class="glyphicon glyphicon-pencil"></span> UPDATE MATERI', ['update', 'id' => $model->id_materi], ['class' => 'btn btn-success']);
                                    echo Html::a('<span class="glyphicon glyphicon-trash"></span> HAPUS', ['delete', 'id' => $model->id_materi], ['class' => 'btn btn-danger','data' => [
                                            'confirm' => 'Apakah anda yakin menghapus data ini?',
                                            'method' => 'post',
                                        ],
                                    ]);
                                } 
                                 
                            ?>
                            
                        </td>
                    </tr>
                </table>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table">
                    <tr>
                        <th>Deskripsi Materi</th>
                    </tr>
                    <tr>
                        <td>
                            <?= $model->deskripsi_materi; ?>
                        </td>
                    </tr>
                     <tr>
                        <th>Download File Materi :  
                        <?php
                            $url = Url::base().'/upload/materi/'.$model->file_materi;
                            echo Html::a(''.$model->judul_materi, $url, ['class' => '','target' => '_blank']); 
                        ?>
                        </th>
                    </tr>
                    
                </table>
                
            </div>
            <div class="box-footer with-border">
                <p align="right">
                    <i>Last Update</i> : <?=$model->updated_at;?>
                    <br>
                    <i>Update By</i> : <?=$model_user->nama_user;?>
                </p>
            </div>
        </div>
    </div>
</div>
