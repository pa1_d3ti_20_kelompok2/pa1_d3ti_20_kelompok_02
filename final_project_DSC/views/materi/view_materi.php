<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Upload Materi';

//$this->params['breadcrumbs'][] = $this->title;
?>

<?php
        
    Modal::begin([
        'header' => '<center><h4>DATA</h4></center>',
        'id' => 'modal-data',
        'size' => 'modal-md',
        'options' => [
              'tabindex' => false, 
          ],
    ]);

    echo '<div id="content-modal-data"></div>';
    Modal::end();
?>

<div class="kategori-index">
    <div class="row">
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="box box-success">
                <div class="box-header with-border">
                    <table class="table">
                        <tr>
                            <td colspan="2" align="center"><h4>MATERI <b class="text-success"><?= $kategori->nama_kategori;?></b> TAHUN <?= $setting->tahun_aktif;?></h4></td>
                            
                            <td align="right">
                                <?php
                                    if($role->item_name == 'PEMBINA' || $role->item_name == 'ADMINISTRATOR' || $role->item_name == 'PENGURUS'){
                                        echo Html::a('<span class="glyphicon glyphicon-cloud-upload"></span> UPLOAD MATERI', ['materi/create','id_kategori'=>$_GET['id_kategori']], ['class' => 'btn btn-success']);
                                    } 
                                     
                                ?>
                            </td>
                        </tr>
                        <tr></tr>
                    </table>
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php
                        if(is_null($materi)){
                            echo "Materi Saat Ini Belum Ada";
                        }else{
                            $i = 1;
                            foreach ($materi as $key => $value) {
                                                           
                    ?>
                            <ul class="products-list product-list-in-box">
                                <li class="item">
                                  <div class="product-img">
                                    <i class="fa fa-book fa-3x"></i>
                                  </div>
                                  <div class="product-info">
                                    <?= Html::a('<h4>'.$value['judul_materi'].'</h4>', ['materi/view','id'=>$value['id_materi']], ['class' => '']) ?>

                                    <a href="javascript:void(0)" class="product-title">
                                      <span class="label label-info pull-right"><?= "Materi ".$i;?></span></a>
                                    <span class="product-description">
                                        <?php
                                            $url = Url::base().'/upload/materi/'.$value['file_materi'];
                                            echo Html::a('<i class="fa fa-download"></i> Download File Materi', $url, ['class' => 'btn btn-default btn-sm','target' => '_blank']) 
                                        ?>
                                    </span>
                                  </div>
                                </li>
                               
                            </ul>
                    <?php
                                $i++;
                            }        
                        }
                    ?>

                </div>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="box box-solid box-success">
                <div class="box-header with-border">
                    <center>
                      FILTER DAN KETERANGAN MATERI
                    </center>
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table">
                        <tr>
                            <th>Filter Materi</th>
                        </tr>
                        <tr>
                            <td>
                                <?php
                                    echo Select2::widget([
                                        'name' => 'id_tahun',
                                        'data' => $arrayTahun,
                                        'options' => [
                                            'placeholder' => 'Filter Data Berdasarkan Tahun ...',
                                            'id'=>'id_tahun'
                                        ],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                    ]);
                                ?>
                            </td>
                        </tr>

                        <tr>
                            <th>Keterangan Materi</th>
                        </tr>
                        <tr align="center">
                            <td><b class="text-success"><?= $kategori->nama_kategori;?> - TAHUN <?= $setting->tahun_aktif;?></b></td>
                        </tr>

                        <tr>
                            <th>Pembina</th>
                        </tr>
                    <?php
                        foreach ($pembina as $key => $value) {
                    ?>
                        <tr align="center">
                            <td><h3 class="label label-primary"><?= $value['nama_user'];?></h3></td>
                        </tr>
                    <?php        
                        }
                    ?>
                        
                    </table>
                    
                </div>
            </div>

            <div class="box box-solid box-danger">
                <div class="box-header with-border">
                    <center>
                      DAFTAR ANGGOTA
                    </center>
                    
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table">
                        <tr>
                            <th>Jumlah Anggota : <?= Html::a($countAnggota.' orang', ['pengurus/list-user','role'=>'ANGGOTA'], ['class' => 'text-danger','title' => 'LIHAT DAFTAR ANGGOTA KESELURUHAN','data-toggle' => 'tooltip',]) ?></th>
                        </tr>
                        <tr>
                       
                    </table>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'nim',
                            'nama_user',
                            
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>

</div>

<?php
 
$script = <<< JS
    $(function () {
        $('#modalButton').click(function () {
            $('#modal-data').modal('show')
                .find('#content-modal-data')
                .load($(this).attr('value'));
        });
        
    });
    
    
    
JS;
$this->registerJs($script)
?>