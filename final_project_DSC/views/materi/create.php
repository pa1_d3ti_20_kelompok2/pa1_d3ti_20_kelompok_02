<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Materi */

$this->title = 'Upload Materi';

?>
<div class="materi-create">

    <?= $this->render('_form', [
        'model' => $model,
        'kategori' =>$kategori,
    ]) ?>

</div>
