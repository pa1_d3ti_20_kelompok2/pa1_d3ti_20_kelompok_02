<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Materi */

$this->title = 'Update Materi';

?>
<div class="materi-update">

    <?= $this->render('_form', [
        'model' => $model,
        'kategori' =>$kategori,
    ]) ?>

</div>
