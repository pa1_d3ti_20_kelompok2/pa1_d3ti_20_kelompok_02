<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use dosamigos\ckeditor\CKEditor;
use skeeks\yii2\ckeditor\CKEditorWidget;
use skeeks\yii2\ckeditor\CKEditorPresets;

/* @var $this yii\web\View */
/* @var $model app\models\Materi */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="materi-form">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <center><h3>FORM UPLOAD MATERI <span class="text-success"><?= $kategori->nama_kategori;?></span></h3></center>
                    <p align="right">
                        <?= Html::a('<span class="glyphicon glyphicon-step-backward"></span>Kembali', ['materi/view-materi','id_kategori'=> $kategori->id_kategori], ['class' => 'btn btn-default']) ?>
                    </p>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php $form = ActiveForm::begin([
                        /*'action' => ['materi/create','id_kategori'=>$kategori->id_kategori],
                        'method' => 'post',*/
                    ]); ?>

                    <?= $form->field($model, 'judul_materi')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'deskripsi_materi')->widget(CKEditorWidget::className(), [
                        'options' => ['rows' => 4],
                        'preset' => CKEditorPresets::FULL
                    ]) ?>

                    <?= $form->field($model, 'file')->widget(FileInput::classname(), 
                        [
                            'pluginOptions'=>[
                                'allowedFileExtensions'=>['jpg','jpeg','png','pdf','docx','doc','zip','rar'],
                                'dropZoneEnabled'=> false,
                                'browseLabel' =>  'Upload File Materi'
                            ],
                        ]) 
                    ?>

    

                    

                    <div class="form-group">
                        <center><?= Html::submitButton('SIMPAN MATERI', ['class' => 'btn btn-success']) ?></center>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
    

</div>
