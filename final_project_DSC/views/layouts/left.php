<?php
    //use hscstudio\mimin\components\Mimin;
    use app\modules\systemxadmin\components\Mimin;
?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                
                <?php 
                    if(!Yii::$app->user->isGuest){
                        if(Yii::$app->user->identity->foto!=null){
                            $urlImage = Yii::getAlias('@web').'/foto_profil/'.Yii::$app->user->identity->foto;

                ?>
                            <img src="<?= $urlImage;?>" class="img-circle" alt="User Image" style="height: 50px;width: 50px;"/>
                <?php
                        }else{
                            $urlImage = Yii::getAlias('@web').'/images/profil.png';
                ?>
                            <img src="<?= $urlImage;?>" class="img-circle" alt="User Image" style="height: 40px;width: 40px;"/>
                <?php            
                        }
                        
                    }
                ?>
            </div>
            <div class="pull-left info">
                <p>
                    <?php 
                        if(!Yii::$app->user->isGuest){
                            echo Yii::$app->user->identity->username;
                        }else{
                            echo "Guest";
                        }
                    ?>

                    


                </p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <?php
            $user = Yii::$app->user->identity;
            $menuItems[] = ['label' => 'MENU', 'options' => ['class' => 'header']];

            if (\Yii::$app->user->isGuest){
                $menuItems[] = ['label' => 'LOGIN', 'url' => ['/site/login']];
                
            }
            else{
                
                $menuItems[] = ['label' => 'HOME', 'icon' => 'home', 'url' => ['site/index']];
                
                $menuItems[] = ['label' => 'MATERI', 'icon' => 'file-text-o', 'url' => ['kategori/home-kategori-materi']];

                $menuItems[] = ['label' => 'TANTANGAN', 'icon' => 'database', 'url' => ['tantangan/index']];

                $menuItems[] = ['label' => 'ADRT', 'icon' => 'exchange', 'url' => ['adrt/view']];
                
                $menuItems[] = ['label' => 'DAFTAR USER', 'icon' => 'users', 'url' => ['pengurus/home-daftar-user']];

                $menuItems[] = ['label' => 'PROGRAM KERJA', 'icon' => 'dashboard', 'url' => ['program-kerja/index']];

                $menuItems[] = ['label' => 'VERIFIKASI ANGGOTA', 'icon' => 'address-card', 'url' => ['pengurus/list-verifikasi-pendaftar']];

                $menuItems[] = [
                        'label' => 'MANAJEMEN DATA',
                        'icon' => 'bars',
                        'url' => '#',
                        'items' => [
                            ['label' => 'DATA KATEGORI', 'icon' => 'database', 'url' => ['kategori/index'],],    
                        ],
                    ];    

                $menuItems[] = 
                    ['label' => 'ACCESS CONTROL','icon'=>'th-large' ,'items' => [
                        ['label' => 'ROUTE', 'icon' => 'asterisk', 'url' => ['/systemxadmin/route/index'],],
                        ['label' => 'ROLE', 'icon' => 'bars', 'url' => ['/systemxadmin/role/index'],],
                        ['label' => 'USER', 'icon' => 'drivers-license-o', 'url' => ['/systemxadmin/user/index'],],
                        
                    ],
                ];
                
                
            }

            $menuItems = Mimin::filterMenu($menuItems);

            echo dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                        'items' => $menuItems,
                    ]
            )
        ?>

      

    </section>

</aside>
