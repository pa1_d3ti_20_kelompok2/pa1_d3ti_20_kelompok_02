<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

?>

<header class="main-header">

    <?= Html::a('<span class="logo-mini">DSC</span><span class="logo-lg">' . "DEL DATA SCIENCE" . '</span>', Yii::$app->homeUrl, ['class' => 'logo']) ?>

    <nav class="navbar navbar-static-top" role="navigation">

        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php 
                    if(!Yii::$app->user->isGuest){
                        if(Yii::$app->user->identity->foto!=null){
                            $urlImage = Yii::getAlias('@web').'/foto_profil/'.Yii::$app->user->identity->foto;

                    ?>
                                <img src="<?= $urlImage;?>" class="img-circle" alt="User Image" style="height: 20px;width: 20px;"/>
                    <?php
                            }else{
                                $urlImage = Yii::getAlias('@web').'/images/profil.png';
                    ?>
                                <img src="<?= $urlImage;?>" class="img-circle" alt="User Image" style="height: 18px;width: 18px;"/>
                    <?php            
                            }
                            
                        }
                    ?>
                        <span class="hidden-xs">
                           
                                <?php 
                                    if(!Yii::$app->user->isGuest){
                                       echo Yii::$app->user->identity->username;
                                        
                                    }else{
                                        echo "Guest";
                                    }
                                ?>

                           
                        </span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <?php
                                if(!Yii::$app->user->isGuest){
                                    if(Yii::$app->user->identity->foto!=null){
                                        $urlImage = Yii::getAlias('@web').'/foto_profil/'.Yii::$app->user->identity->foto;

                            ?>
                                        <img src="<?= $urlImage;?>" class="img-circle" alt="User Image" style="height: 160px;width: 160px;"/>
                            <?php
                                    }else{
                                        $urlImage = Yii::getAlias('@web').'/images/profil.png';
                            ?>
                                        <img src="<?= $urlImage;?>" class="img-circle" alt="User Image" style="height: 160px;width: 160px;"/>
                            <?php            
                                    }
                                    
                                }
                            ?>
                            
                        </li>
                       
                        
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a(
                                    'Profile',
                                    ['/site/view-profile','id'=>Yii::$app->user->identity->id],
                                    ['class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    'Sign out',
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>

                <!-- User Account: style can be found in dropdown.less -->
                <li>
                    <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>
