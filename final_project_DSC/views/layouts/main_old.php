<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use hscstudio\mimin\components\Mimin;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
            $user = Yii::$app->user->identity;
            //$menuItems[] = ['label' => 'MENU', 'options' => ['class' => 'header']];

            if (\Yii::$app->user->isGuest){
               
                $menuItems[] = ['label' => 'HOME', 'url' => ['/site/index']]; 
                $menuItems[] = ['label' => 'LOGIN', 'url' => ['/site/login']];
                $menuItems[] = ['label' => 'DAFTAR', 'url' => ['/site/register']];

                
            }
            else{
                
                $menuItems[] = ['label' => 'HOME', 'icon' => 'home', 'url' => ['site/index']];

                $menuItems[] = ['label' => 'VOTING', 'icon' => 'home', 'url' => ['hasil-voting/online-voting']];
                
                
                $menuItems[] = 
                    ['label' => 'DATA VOTING','icon'=>'th-large' ,'items' => [
                        ['label' => 'MASTER DATA VOTING', 'icon' => 'asterisk', 'url' => ['/voting/index'],],
                        ['label' => 'HASIL VOTING', 'icon' => 'bars', 'url' => ['/voting/index-hasil-voting'],],
                        ['label' => 'REKAPITULASI VOTING USER', 'icon' => 'bars', 'url' => ['/voting/index-pengguna-voting'],],
                    ],
                ];

                $menuItems[] = 
                    ['label' => 'ACCESS CONTROL','icon'=>'th-large' ,'items' => [
                        ['label' => 'ROLE', 'icon' => 'bars', 'url' => ['/mimin/role/index'],],
                        ['label' => 'USER', 'icon' => 'drivers-license-o', 'url' => ['/mimin/user/index'],],
                        ['label' => 'ROUTE', 'icon' => 'asterisk', 'url' => ['/mimin/route/index'],],
                        
                    ],
                ];

                $menuItems[] = ['label' => 'Logout ('.Yii::$app->user->identity->username.')', 'url' => ['/site/logout'], 'linkOptions' => ['data-method' => 'post'],];
            
                
                    
            }

            $menuItems = Mimin::filterMenu($menuItems);

        ?>
    <?php
    NavBar::begin([
        'brandLabel' => '<b><i class="text-primary">Online Voting System</i></b>',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Developer Team of D3TI (Nama_Kelompok) <?= date('Y') ?></p>

        <p class="pull-right">Web Programming Project</p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
